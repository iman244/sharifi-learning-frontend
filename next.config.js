/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "ieltsonlineshopping.s3.ir-thr-at1.arvanstorage.ir",
        port: "",
        pathname: "/**",
      },
      {
        protocol: "https",
        hostname: "s3.ir-thr-at1.arvanstorage.ir",
        port: "",
        pathname: "/**",
      },
    ],
  },
};

module.exports = nextConfig;
