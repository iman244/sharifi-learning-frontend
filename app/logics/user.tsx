import { createContext, useEffect, useReducer, useState } from "react";
import { user } from "../types/backend/entities";
import { useContainer } from "../infrastructure/hooks/useContainer";
import { login, loginInputs, logout } from "../types/front/types";
import { useRouter } from "next/navigation";
import useGet from "../infrastructure/hooks/useGet";
import usePost from "../infrastructure/hooks/usePost";

type haveProductType = (productName: string) => boolean;

export const UserContext = createContext<{
  user: null | user;
  login?: login;
  logout?: logout;
  haveProduct?: haveProductType;
  userGetLoading?: boolean
  refreshUser?: () => void
}>({
  user: null,
  userGetLoading: true
});

function reducer(state: number, action: { type: "refresh" }) {
  switch (action.type) {
    case "refresh":
      return state + 1;
  }
}

export function UserContextProvider({ children }: { children: any }) {
  const router = useRouter();
  const [user, setUser] = useState<null | user>(null);
  const [refreshToken, dispatch] = useReducer(reducer, 0);
  const [userGetLoading, setUserGetLoading] = useState(true)


  useEffect(()=>{
    if(!localStorage.getItem('Token')) {
      setUserGetLoading(false)
    }
  },[])

  useEffect(()=>{
    if(user) {
      // dispatchEvent(new Event('user login'))
    }
  },[user])

  const userQuery = useGet("auth/verify-user/",["verify", refreshToken], {
    case200: (data: { user: user }) => {
      setUser(data.user);
      setUserGetLoading(false)
    },
     caseError(err) {
      setUserGetLoading(false)
     },
  });


  const refreshUser = () => dispatch({ type: "refresh" });

  const haveProduct: haveProductType = (productName) => {
    const findProducts = user?.products.filter(
      (product) => product.name === productName
    );
    if (findProducts && findProducts.length > 0) {
      return true;
    } else {
      return false;
    }
  };

  function login({ Token, log }: loginInputs) {
    log && console.log("Token", Token);
    localStorage.setItem("Token", Token);
    window.dispatchEvent(new Event('user-login'))
    refreshUser();
  }

  function logout() {
    localStorage.removeItem('Token');
    localStorage.removeItem('phone_number');
    setUser(null);
    window.dispatchEvent(new Event('user-logout'))
    router.push("/");
  }

  return (
    <UserContext.Provider value={{ user, login, userGetLoading, logout, haveProduct, refreshUser }}>
      {children}
    </UserContext.Provider>
  );
}
