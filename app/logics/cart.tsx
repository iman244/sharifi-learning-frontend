import {
  createContext,
  useContext,
  useEffect,
  useReducer,
  useState,
} from "react";
import { cart, product } from "../types/backend/entities";
import { isInCart } from "../types/front/types";
import usePost from "../infrastructure/hooks/usePost";
import useGet from "../infrastructure/hooks/useGet";
import { UserContext } from "./user";
import { useRouter } from "next/navigation";

export const CartContext = createContext<{
  cart_items: product[];
  offlineCart: string[];
  cart?: cart;
  addProductToCart?: (product: product) => {
    loading: boolean;
  };
  removeProductFormCart?: (product: product) => {
    loading: boolean;
  };
  isInCart?: isInCart;
}>({
  cart_items: [],
  offlineCart: [],
});

function reducer(state: number, action: { type: "refresh" }) {
  switch (action.type) {
    case "refresh":
      return state + 1;
  }
}

export function CartContextProvider({ children }: { children: any }) {
  const [cart, setCart] = useState<cart>();
  const [offlineCart, setOfflineCart] = useState<string[]>([]);
  const [cart_items, setCart_items] = useState<product[]>([]);
  const [refreshCartToken, dispatch] = useReducer(reducer, 0);
  const [cartChange, setCartChange] = useState(0);
  const { user, userGetLoading, refreshUser } = useContext(UserContext);
  const router = useRouter();

  const cartChanged = () => setCartChange((v) => v + 1);

  const GET_CART = useGet("store/cart/", ["cart", refreshCartToken], {
    case200(data: cart) {
      setCart_items(data.products);
      setCart(data);
    },
  });

  const UPDATE_CART = usePost(`store/cart/`, {
    method: "put",
    case200() {
      refreshCart();
    },
  });

  const ADD_FREE_PRODUCT = usePost(`store/free/`, {
    method: "put",
    case200() {
      refreshUser && refreshUser();
    },
  });

  useEffect(() => {
    function LoginWorks() {
      var localStorageCart = localStorage.getItem("cart");
      if (localStorageCart) {
        localStorageCart = JSON.parse(localStorageCart);
        if (Array.isArray(localStorageCart)) {
          if (localStorageCart.length > 0) {
            UPDATE_CART.mutate({
              products: localStorageCart,
              intention: "add",
            });
            localStorage.setItem("cart", JSON.stringify([]));
          }
        }
      } else {
        localStorage.setItem("cart", JSON.stringify([]));
      }
    }
    function LogoutWorks() {
      localStorage.setItem("cart", JSON.stringify([]));
      cartChanged();
    }
    window.addEventListener("user-login", LoginWorks);
    window.addEventListener("user-logout", LogoutWorks);
    return () => {
      window.removeEventListener("user-login", LoginWorks);
      window.removeEventListener("user-logout", LogoutWorks);
    };
  }, []);

  const refreshCart = () => dispatch({ type: "refresh" });

  // cart handling for anonymous user
  useEffect(() => {
    function getCart() {
      var localStorageCart = localStorage.getItem("cart");
      if (localStorageCart) {
        localStorageCart = JSON.parse(localStorageCart);
        if (Array.isArray(localStorageCart)) {
          setOfflineCart(localStorageCart);
        }
      } else {
        localStorage.setItem("cart", JSON.stringify([]));
      }
    }
    getCart();
  }, [cartChange]);

  function isInCart(productName: string) {
    if (!userGetLoading && user) {
      return cart_items.findIndex((v) => v.name === productName) !== -1;
    } else {
      return offlineCart.findIndex((v) => v === productName) !== -1;
    }
  }

  const addProductToCart = (product: product) => {
    if (!userGetLoading && user) {

      if (cart && product.price !== 0) {
        const a = UPDATE_CART.mutate({
          products: [product.name],
          intention: "add",
        });
      } else if (product.price === 0) {
        const a = ADD_FREE_PRODUCT.mutate({
          product: product.name,
        });        
      }

    } else if (product.price === 0) {

      router.push(`/auth?next=store/${product.id}`);
    } else {
      var offlineCart: any = localStorage.getItem("cart");

      if (offlineCart) {
        offlineCart = JSON.parse(offlineCart);
        if (Array.isArray(offlineCart)) {
          let isInCart = offlineCart.findIndex((v) => v === product.name);
          if (isInCart === -1) {
            localStorage.setItem(
              "cart",
              JSON.stringify([...offlineCart, product.name])
            );
            setCartChange(cartChange + 1);
          }
        }
      } else {
        localStorage.setItem("cart", JSON.stringify([product.name]));
      }
    }

    // at end we say the caller that works had been done
    return {
      loading: false,
    };
  };

  function removeProductFormCart(product: product) {
    if (!userGetLoading && user) {
      cart &&
        UPDATE_CART.mutate({ products: [product.name], intention: "remove" });
    } else {
      var offlineCart: any = localStorage.getItem("cart");

      if (offlineCart) {
        if (isInCart(product.name)) {
          offlineCart = JSON.parse(offlineCart);
          if (Array.isArray(offlineCart)) {
            let indexProductInCart = offlineCart.findIndex(
              (v) => v === product.name
            );
            offlineCart.splice(indexProductInCart, 1);
            localStorage.setItem("cart", JSON.stringify(offlineCart));
            setCartChange(cartChange + 1);
          } else {
            console.log("removeProductFormCart cart is not array");
          }
        } else {
          console.log("removeProductFormCart item is not in cart");
        }
      } else {
        console.log("removeProductFormCart cart is null");
      }
    }
    return {
      loading: false,
    };
  }

  return (
    <CartContext.Provider
      value={{
        cart_items,
        cart,
        offlineCart,
        isInCart,
        addProductToCart,
        removeProductFormCart,
      }}
    >
      {children}
    </CartContext.Provider>
  );
}
