import React, { useContext, useEffect, useState } from "react";
import Form from "../infrastructure/Form";
import { Button, Flex, Text } from "@chakra-ui/react";
import { SubmitButton } from "../infrastructure/FormComponents";
import {
  CodeInput,
  EmailInput,
  PasswordInput,
  PhoneNumberInput,
} from "../components/inputs/auth.input";
import { auth_post_data } from "../types/backend/storageAPI";
import { UserContext } from "../logics/user";
import { useRouter, useSearchParams } from "next/navigation";
import { digitsEnToFa } from "@persian-tools/persian-tools";

export const Auth = ({
  log = false,
  nextPath,
}: {
  log?: boolean;
  nextPath?: string;
}) => {
  const [phoneNumber, setPhoneNumber] = useState("");
  const [phase, setPhase] = useState<1 | 2>(1);

  return (
    <Flex
      flexDir={"column"}
      alignItems={"center"}
      gap={"12px"}
      w={"fit-content"}
    >
      {phase == 1 && (
        <RequestCodeForm setPhase={setPhase} setPhoneNumber={setPhoneNumber} />
      )}
      {phase == 2 && (
        <SubmitCodeForm
          setPhase={setPhase}
          nextPath={nextPath}
          phoneNumber={phoneNumber}
        />
      )}

      {phase == 2 && <RequestCodeAgain />}
    </Flex>
  );
};

export const RequestCodeAgain = () => {
  const loadingState = useState(false);
  return (
    <Form
      url={"auth/request-otp-code/"}
      loadingState={loadingState}
      onSubmitF={() => {
        const data = {
          phone_number: localStorage.getItem("phone_number"),
        };
        return data;
      }}
      case200={(data) => {
        return data;
      }}
    >
      <SubmitButton isLoading={loadingState[0]} variant="link">
        درخواست مجدد کد ورود
      </SubmitButton>
    </Form>
  );
};

const PhoneNumberHandling = ({
  phone_number,
  setPhase,
}: {
  phone_number: string;
  setPhase: React.Dispatch<React.SetStateAction<1 | 2>>;
}) => {
  return (
    <Flex
      flexDir={"column"}
      gap={"12px"}
      textAlign={"center"}
      alignItems={"center"}
      justifyContent={"center"}
    >
      <Flex gap={"8px"}>
        <Text fontSize={"sm"}>پیامک ارسال شد به:</Text>
        <Text dir="ltr">{digitsEnToFa(phone_number)}</Text>
      </Flex>
      <Button
        variant={"ghost"}
        size={"sm"}
        onClick={() => {
          setPhase(1);
        }}
      >
        تغییر شماره همراه
      </Button>
    </Flex>
  );
};

const RequestCodeForm = ({
  setPhase,
  setPhoneNumber,
}: {
  setPhase: React.Dispatch<React.SetStateAction<2 | 1>>;
  setPhoneNumber: React.Dispatch<React.SetStateAction<string>>;
}) => {
  const loadingState = useState(false);

  return (
    <Form
      url={"auth/request-otp-code/"}
      loadingState={loadingState}
      onSubmitF={(data: { phone_number: string }) => {
        localStorage.setItem("phone_number", data.phone_number);
        if (data.phone_number[0] == "0") {
          let convertedPhoneNumber = "+98" + data.phone_number.slice(1);
          setPhoneNumber(convertedPhoneNumber);
        } else if (data.phone_number[0] == "+") {
          setPhoneNumber(data.phone_number);
        } else {
          // we must show error
          setPhoneNumber(data.phone_number);
        }
        return data;
      }}
      case200={(data) => {
        setPhase(2);

        return data;
      }}
    >
      <Flex flexDir={"column"} gap={"12px"} dir="rtl" alignItems={"center"}>
        <PhoneNumberInput w="250px" />
        <SubmitButton
          isLoading={loadingState[0]}
          width={"100%"}
          bgColor={"gray.200"}
        >
          ورود/عضویت
        </SubmitButton>
      </Flex>
    </Form>
  );
};

const SubmitCodeForm = ({
  nextPath,
  phoneNumber,
  setPhase,
}: {
  nextPath: string | undefined;
  phoneNumber: string;
  setPhase: React.Dispatch<React.SetStateAction<2 | 1>>;
}) => {
  const loadingState = useState(false);
  const userLogics = useContext(UserContext);
  const router = useRouter();
  const searchParams = useSearchParams();
  const [next, setNext] = useState(searchParams.get("next"));

  return (
    <Form
      url={"auth/verify-otp-code/"}
      loadingState={loadingState}
      onSubmitF={(data: { phone_number: string }) => {
        return {
          ...data,
          phone_number: localStorage.getItem("phone_number"),
        };
      }}
      case200={(data) => {
        if( data.token !== undefined ){
          userLogics.login && userLogics.login({ Token: data.token });
          if (!!nextPath) {
            router.push(nextPath);
          } else if (next) {
            router.push(next);
          } else {
            router.push("/");
          }
        } else {
          console.log("we are in undefined")
        }
        
        return data;
      }}
    >
      <Flex flexDir={"column"} gap={"12px"} dir="rtl" alignItems={"center"}>
        {phoneNumber && (
          <PhoneNumberHandling phone_number={phoneNumber} setPhase={setPhase} />
        )}
        <CodeInput w="250px" />

        <SubmitButton
          isLoading={loadingState[0]}
          width={"100%"}
          bgColor={"gray.200"}
        >
          ارسال کد
        </SubmitButton>
      </Flex>
    </Form>
  );
};
