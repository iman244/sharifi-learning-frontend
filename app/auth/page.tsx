"use client";
import React from "react";
import { Auth } from "./Auth";
import { Flex, Heading } from "@chakra-ui/react";
import { Logo } from "../components/Navbar/Logo";

export default function page() {
  return (
    <Flex
      alignItems={"center"}
      justifyContent={"center"}
      w={"100%"}
      flex={1}
      height={"100%"}
      flexDir={"column"}
      gap={"24px"}
    >
      <Heading>فروشگاه آنلاین آیلتس</Heading>
      <Auth />
    </Flex>
  );
}
