import {
  containerInput,
  page,
} from "@/app/infrastructure/types/inf_frontTypes";
import { ButtonProps, StyleProps } from "@chakra-ui/react";

export interface CustomPageLinkProps extends ButtonProps, StyleProps {
  page: page;
  loadingTest?: boolean;
  onLand?: ()=>void;
  children?: any;
}

export interface StorageContainerInput extends containerInput {
  setFindBy: React.Dispatch<
    React.SetStateAction<{
      fileName: string;
      mimeType: string;
    }>
  >;
}

export interface loginInputs {
  Token: string;
  log?: boolean;
}
export type login = (inputs: loginInputs) => void;
export type logout = () => void;

export type isInCart = (productName: string) => boolean;
