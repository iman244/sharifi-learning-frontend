export interface CreateCategoryDto {
  categoryName: string;
}

export interface NewFileDto {
  fileName: string;
  mimeType: string;
}

export interface CreateProductDto {
  productName: string;
  description: string;
  price: number;
  categoryName: string;
  cover: string;
  files: string[];
}
