import { category, file } from "./entities";

type Fields = Record<string, string>;

export interface uploadStorageData {
  message: string;
  PresignedPost: {
    url: string;
    fields: Fields;
  };
}

export interface getStorageFile {
  preSignedURL: string;
}

export interface auth_post_data {
  message: string;
  access_token: string;
}

export type getCategory = category;
export type getCategories = category[];
