export interface category {
  id: number;
  name: string;
  image: file;
  // products: product[];
}

export interface file {
  id: number;
  name: string;
  file: string;
}

export interface product {
  id: number;
  name: string;
  content: string;
  price: number;
  discount: number;
  category: category;
  image: file;
}

export interface product_includeFile extends product {
  file: file;
}

export interface product_owned {
  file: file;
}

export interface user {
  phone_number: string;
  products: product_includeFile[];
  payments: payment[];
}

export interface payment {
  amount: number;
  authority: string;
  id: number;
  paymentStartTime: string;
  products: product[];
  status: boolean;
  user: number;
}

export interface cart {
  id: number;
  products: [];
  status: boolean;
  user: { phone_number: string };
}
