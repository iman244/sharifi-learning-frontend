"use client";

import React from "react";
// import { QueryClientProvider, QueryClient } from "react-query";
// import { ReactQueryStreamedHydration } from "@tanstack/react-query-next-experimental";
import { ChakraProvider } from "@chakra-ui/react";
import { QueryClient, QueryClientProvider } from "react-query";
import theme from "./infrastructure/theme";
import { UserContextProvider } from "./logics/user";
import { CartContextProvider } from "./logics/cart";

const queryClient = new QueryClient();
function Providers({ children }: React.PropsWithChildren) {
  // const [client] = React.useState(new QueryClient());

  return (
    <ChakraProvider theme={theme}>
      <QueryClientProvider client={queryClient}>
        <UserContextProvider>
          <CartContextProvider>
            {/* <ReactQueryStreamedHydration queryClient={queryClient}> */}
            {children}
            {/* </ReactQueryStreamedHydration> */}
          </CartContextProvider>
        </UserContextProvider>
      </QueryClientProvider>
    </ChakraProvider>
  );
}

export default Providers;
