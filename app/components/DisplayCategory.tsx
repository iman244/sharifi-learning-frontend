import { Flex, Image, Spinner, Text } from "@chakra-ui/react";
// import Image from "next/image";
import NextLink from "next/link";
import React, { useState } from "react";
import { category } from "../types/backend/entities";
import { CardLoading } from "./CardLoading";

export const DisplayCategory = ({ category }: { category: category }) => {
  const [isLoading, setIsLoading] = useState(false);

  const handleOnClick = () => {
    setIsLoading(true);
  };

  return (
    <Flex flexDir={'column'} gap={'24px'} alignItems={'center'}>

    <Flex
      bgColor={"gray.200"}
      alignItems={"center"}
      justifyContent={"center"}
      as={NextLink}
      href={`/store?categoryName=${category.name}`}
      position={"relative"}
      borderRadius={"16px"}
      overflow={"hidden"}
      boxShadow={"lg"}
      w={"267px"}
      height={"375px"}
      onClick={handleOnClick}
      cursor={"pointer"}
      _hover={{ boxShadow: "2xl" }}
      transition={"all 0.3s linear"}
      
      >
      {isLoading && <CardLoading />}
      {/* <Image src={category.image} alt={category.name}   width={"267px"}
        height={"375px"} /> */}
      <Image
        width={267}
        height={375}
        alt={category.image.name}
        src={`${category.image.file}`}
        />
      
    </Flex>
    <Text fontSize={'larger'} fontWeight={'500'}>{category.name}</Text>
        </Flex>
  );
};
