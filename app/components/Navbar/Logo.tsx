import Image from "next/image";
import Link from "next/link";
import React from "react";

export const Logo = () => {
  return (
    <Link href="/">
      <Image
        alt="logo"
        src={"/sharifi-learning-logo.png"}
        width={60}
        height={60}
      />
    </Link>
  );
};
