"use client";

import {
  Button,
  Drawer,
  DrawerBody,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  Flex,
  Icon,
  Spacer,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import React from "react";
import Link from "next/link";
import { CartLink } from "./CartLink";
import Image from "next/image";
import { ProfileLink } from "./ProfileLink";
import { AdminPanel } from "./AdminPrivilage";
import { GiHamburgerMenu } from "react-icons/gi";
import { Logo } from "./Logo";
import { LogOut } from "./LogOut";
import { StoreLink } from "./StoreLink";

export const Navbar = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <Flex
      alignItems={"center"}
      justifyContent={"space-between"}
      h={"100%"}
      dir="rtl"
    >
      <Flex alignItems={"center"} gap={"12px"}>
        <Button onClick={onOpen} variant={"link"} color={"black"}>
          <Icon as={GiHamburgerMenu} boxSize={5} />
        </Button>
        <Logo />
      </Flex>
      <Flex alignItems={"center"} gap={"12px"}>
        <ProfileLink />
        <StoreLink  />
        <CartLink />
      </Flex>
      <Sidebar onClose={onClose} isOpen={isOpen} />
    </Flex>
  );
};

const Sidebar = ({
  onClose,
  isOpen,
}: {
  onClose: () => void;
  isOpen: boolean;
}) => {
  return (
    <Drawer placement="right" onClose={onClose} isOpen={isOpen}>
      <DrawerOverlay />
      <DrawerContent gap={"24px"} minH={"100vh"} paddingBottom={"36px"}>
        <DrawerHeader borderBottomWidth="1px" dir="rtl">
          <Flex
            gap={"12px"}
            alignItems={"center"}
            onClick={onClose}
            cursor={"pointer"}
          >
            <Logo />
            <Text>فروشگاه آنلاین آیلتس</Text>
          </Flex>
        </DrawerHeader>
        <DrawerBody dir="rtl">
          <Flex flexDir={"column"} gap={"12px"}>
            <ProfileLink sidebar={true} onLand={onClose} />
            <StoreLink sidebar={true} onLand={onClose} />
            <CartLink responsive={true} onLand={onClose} />
            <Spacer />
          </Flex>
        </DrawerBody>
        <Spacer />
        <Flex w={"100%"} paddingX={"1.5rem"}>
          <LogOut onLogout={onClose} />
        </Flex>
      </DrawerContent>
    </Drawer>
  );
};
