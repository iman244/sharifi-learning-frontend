"use client";
import React, { useContext, useEffect, useState } from "react";
import { CustomPageLink } from "../CustomPageLink";
import { Box, Flex, Icon, Text } from "@chakra-ui/react";
import { RiLuggageCartFill } from "react-icons/ri";
import { digitsEnToFa } from "@persian-tools/persian-tools";
import { CartContext } from "@/app/logics/cart";
import { UserContext } from "@/app/logics/user";

export const CartLink = ({
  responsive = false,
  onLand,
}: {
  responsive?: boolean;
  onLand?: () => void;
}) => {
  const { cart_items, offlineCart } = useContext(CartContext);
  const { user, userGetLoading } = useContext(UserContext);

  return (
    <CustomPageLink
      page={{ path: "cart" }}
      minW={{ base: "50px", sm: "130px" }}
      w={responsive ? "100%" : "fit-content"}
      onLand={() => {
        onLand && onLand();
      }}
    >
      <Flex gap={"8px"} alignItems={"center"}>
        <Text display={{ base: "none", sm: "block" }}>سبد خرید</Text>

        <Flex position={"relative"}>
          {!userGetLoading && user ? (
            cart_items.length > 0 ? (
              <Badage value={digitsEnToFa(cart_items.length)} />
            ) : (
              ""
            )
          ) : offlineCart.length > 0 ? (
            <Badage value={digitsEnToFa(offlineCart.length)} />
          ) : (
            ""
          )}
          <Icon zIndex={"5"} boxSize={6} as={RiLuggageCartFill} />
        </Flex>
      </Flex>
    </CustomPageLink>
  );
};

const Badage = ({ value }: { value: string }) => {
  return (
    <Flex
      top={"-5px"}
      right={"-10px"}
      position={"absolute"}
      borderRadius={"50%"}
      bgColor={"blue.300"}
      w={"20px"}
      h={"20px"}
      lineHeight={"20px"}
      justifyContent={"center"}
      alignItems={"center"}
    >
      {value}
    </Flex>
  );
};
