"use client";
import React from "react";
import { CustomPageLink } from "../CustomPageLink";
import {  Flex,  Icon,  Text } from "@chakra-ui/react";
import { ImLibrary } from "react-icons/im";

export const StoreLink = ({
  sidebar = false,
  onLand,
}: {
  responsive?: boolean;
  sidebar?: boolean;
  onLand?: () => void;
}) => {

  return (
    <CustomPageLink
      page={{ path: "store" }}
      minW={{ base: "50px", sm: "130px" }}
      w={sidebar ? "100%" : "fit-content"}
      onLand={() => {
        onLand && onLand();
      }}
    >
          <Flex gap={"8px"}>
            <Icon as={ImLibrary} />
            <Text display={{base: 'none', sm: 'block'}}>فروشگاه</Text>
          </Flex>
    </CustomPageLink>
  );
};

