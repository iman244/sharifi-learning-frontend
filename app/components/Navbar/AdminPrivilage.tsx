"use client";
import { Authorization } from "@/app/infrastructure/Authorization";
import { Flex, Text } from "@chakra-ui/react";
import React from "react";
import { CustomPageLink } from "../CustomPageLink";

export const AdminPanel = () => {
  return (
    <Authorization
      roles={["admin"]}
      render={(user) => (
        <Flex
          // w={"fit-content"}
          // bgColor={"gray.400"}
          gap={"12px"}
          // alignItems={"flex-end"}
          padding={"8px"}
          boxSizing={"border-box"}
          borderRadius={"md"}
          flexDir={"column"}
        >
          <Flex
            bgColor={"gray.200"}
            alignItems={"center"}
            padding={"8px 16px"}
            borderRadius={"md"}
          >
            <Text>پنل ادمین</Text>
          </Flex>
          {[
            {
              path: "adminpanel/user",
              label: "حساب‌های کاربری",
            },
            {
              path: "adminpanel/storage",
              label: "فایل‌ها",
            },
            {
              path: "adminpanel/category",
              label: "دسته‌بندی‌ها",
            },
            {
              path: "adminpanel/store",
              label: "محصولات",
            },
          ].map((item) => (
            <CustomPageLink
              key={item.path}
              page={{
                path: item.path,
              }}
            >
              {item.label}
            </CustomPageLink>
          ))}
        </Flex>
      )}
    />
  );
};
