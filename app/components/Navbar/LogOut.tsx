import { Authorization } from "@/app/infrastructure/Authorization";
import { UserContext } from "@/app/logics/user";
import { Button, Flex, Icon, Text } from "@chakra-ui/react";
import React, { useContext } from "react";
import { BiLogOut } from "react-icons/bi";

export const LogOut = ({onLogout}: {onLogout?: ()=> void}) => {
  const {logout} = useContext(UserContext)
  return (
    <Authorization
      render={(user) => {
        return (
          <Button
          w={'100%'}
          colorScheme="red"
            gap={"12px"}
            alignItems={"center"}
            leftIcon={<Icon as={BiLogOut} boxSize={5} />}
            onClick={()=>{

              logout && logout()
              onLogout && onLogout()
            }}
          >
            <Text>خروج از حساب</Text>
          </Button>
        );
      }}
    />
  );
};
// () => {
//   localStorage.clear();
// }