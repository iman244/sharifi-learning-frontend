"use client";
import React from "react";
import { CustomPageLink } from "../CustomPageLink";
import { Flex, Icon, Text, useBreakpointValue } from "@chakra-ui/react";
import { Authorization } from "@/app/infrastructure/Authorization";
import { user } from "@/app/types/backend/entities";
import { FaUser, FaUserPlus } from "react-icons/fa";
import { usePathname, useRouter } from "next/navigation";

export const ProfileLink = ({ sidebar = true, onLand }: { sidebar?: boolean; onLand?: ()=>void }) => {
  const pathname = usePathname();

  const signUp = useBreakpointValue({
    base: <Icon as={FaUserPlus} />,
    md: "ورود / عضویت",
  });
  return (
    <Authorization
      id="profileLink"
      notValid={
        <CustomPageLink
          page={{
            path: `auth?next=${pathname}`,
          }}
          minW={{ base: "50px", md: "120px" }}
          w={sidebar ? "100%" : "fit-content"}
        >
          {sidebar ? signUp : "ورود / عضویت"}
          
        </CustomPageLink>
      }
      render={(user) => (
        <CustomPageLink
          w={sidebar ? "100%" : "fit-content"}
          page={{
            path: "profile",
          }}
          onLand={()=>{
            onLand && onLand()
          }}
          minW={{ base: "0", md: "120px" }}
        >
          <Flex gap={"8px"}>
            <Icon as={FaUser} />
            <Text>پروفایل</Text>
          </Flex>
        </CustomPageLink>
      )}
    />
  );
};

const UserPhoneNumber = ({
  user,
  sidebar = true,
  log = false,
}: {
  user: user;
  sidebar?: boolean;
  log?: boolean;
}) => {
  log && console.log("user: ", user);

  return (
    <CustomPageLink
      page={{
        path: "profile",
      }}
      minW={{ base: "0", md: "120px" }}
    >
      <Flex gap={"8px"}>
        <Icon as={FaUser} />
        <Text>پروفایل</Text>
      </Flex>
    </CustomPageLink>
  );
};
