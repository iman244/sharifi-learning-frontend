import Form, { ConnectForm } from "@/app/infrastructure/Form";
import {
  ComplicatedInput,
  SubmitButton,
} from "@/app/infrastructure/FormComponents";
import {
  Button,
  Flex,
  Icon,
  InputGroup,
  InputLeftElement,
  Text,
} from "@chakra-ui/react";
import { useState } from "react";
import { useFormContext } from "react-hook-form";
import { BiHide, BiShow } from "react-icons/bi";

export const EmailInput = ({ w }: { w?: string }) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          name={"email"}
          placeholder={"ایمیل"}
          rules={{
            required: {
              value: true,
              message: "ایمیل را وارد کنید",
            },
            pattern: {
              value: /^\S+@\S+$/i,
              message: "ایمیل صحیح نیست",
            },
          }}
          w={w}
        />
      )}
    </ConnectForm>
  );
};

export const PhoneNumberInput = ({ w }: { w?: string }) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          bgColor={"white"}
          register={register}
          name={"phone_number"}
          placeholder={"شماره همراه"}
          rules={{
            required: {
              value: true,
              message: "شماره همراه خود را وارد کنید",
            },
            pattern: {
              value: /^[0-9]+$/i,
              message: "شماره همراه صحیح نیست",
            },
          }}
          type="number"
          w={w}
        />
      )}
    </ConnectForm>
  );
};

export const PasswordInput = ({ w = "216px" }: { w?: string }) => {
  const [show, setShow] = useState(false);
  const ShowHideClick = () => setShow(!show);

  return (
    <ConnectForm>
      {({ register }: any) => (
        <InputGroup size="md">
          <ComplicatedInput
            bgColor={"white"}
            rules={{
              required: {
                value: true,
                message: "لطفاً رمز عبور خود را وارد کنید",
              },
              minLength: {
                value: 6,
                message: "رمز عبور باید حداقل 6 حرف باشد",
              },
            }}
            name="password"
            register={register}
            placeholder="رمز عبور"
            type={show ? "text" : "password"}
            w={w}
          />
          <InputLeftElement
            width="3rem"
            mt={"28px"}
            _focusVisible={{
              outline: "none",
            }}
          >
            <Button size="sm" onClick={ShowHideClick} variant={"link"}>
              {show ? <Icon as={BiHide} /> : <Icon as={BiShow} />}
            </Button>
          </InputLeftElement>
        </InputGroup>
      )}
    </ConnectForm>
  );
};

export const NewPasswordInput = ({ w = "216px" }: { w?: string }) => {
  const [show, setShow] = useState(false);
  const ShowHideClick = () => setShow(!show);

  return (
    <ConnectForm>
      {({ register }: any) => (
        <InputGroup size="md">
          <ComplicatedInput
            bgColor={"white"}
            rules={{
              required: {
                value: true,
                message: "لطفاً رمز عبور جدید خود را وارد کنید",
              },
              minLength: {
                value: 6,
                message: "رمز عبور جدید باید حداقل 6 حرف باشد",
              },
            }}
            name="newPassword"
            register={register}
            placeholder="رمز عبور جدید"
            type={show ? "text" : "password"}
            w={w}
          />
          <InputLeftElement
            width="3rem"
            mt={"28px"}
            _focusVisible={{
              outline: "none",
            }}
          >
            <Button size="sm" onClick={ShowHideClick} variant={"link"}>
              {show ? <Icon as={BiHide} /> : <Icon as={BiShow} />}
            </Button>
          </InputLeftElement>
        </InputGroup>
      )}
    </ConnectForm>
  );
};

export const UpdatePasswordInput = () => {
  return (
    <Flex gap={"12px"} flexDir={"column"}>
      <PasswordInput />
      <NewPasswordInput />
    </Flex>
  );
};

export const IsAdminInput = () => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          name="isAdmin"
          register={register}
          type={"checkbox"}
          placeholder="ادمین"
        />
      )}
    </ConnectForm>
  );
};

export const CodeInput = ({ w = "216px" }: { w?: string }) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          bgColor={"white"}
          register={register}
          name={"code"}
          placeholder={"کد ورود"}
          rules={{
            required: {
              value: true,
              message: "لطفاً کد پیامک‌شده را وارد کنید",
            },
          }}
          type="number"
          w={w}
        />
      )}
    </ConnectForm>
  );
};
