import { Flex, Spinner } from "@chakra-ui/react";
import React from "react";

export const CardLoading = () => {
  return (
    <Flex
      alignItems={"center"}
      justifyContent={"center"}
      zIndex={1}
      position={"absolute"}
      width={"100%"}
      height={"100%"}
      bgColor={"gray.500"}
      opacity={0.7}
    >
      <Spinner size={"xl"} thickness="3px" />
    </Flex>
  );
};
