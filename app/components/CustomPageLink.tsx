"use client";
import { usePathname } from "next/navigation";
import React, { useEffect, useState } from "react";
import { page } from "../infrastructure/types/inf_frontTypes";
import { Button } from "@chakra-ui/react";
import NextLink from "next/link";
import { CustomPageLinkProps } from "../types/front/types";

export const CustomPageLink = ({
  page,
  loadingTest = false,
  children,
  onLand,
  ...rest
}: CustomPageLinkProps) => {
  const pathname = usePathname();

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    let QueryStartIndex = page.path.indexOf("?");

    let path =
      QueryStartIndex !== -1 ? page.path.slice(0, QueryStartIndex) : page.path;
    
    if (pathname === `/${path}`) {
      setIsLoading(false);
    }
  }, [pathname]);
  return (
    <Button
      dir="rtl"
      w={"fit-content"}
      _hover={{ bgColor: "gray.300" }}
      bgColor={"gray.200"}
      border={"none"}
      minW={"120px"}
      padding={"12px"}
      variant={pathname === `/${page.path}` ? "customLinkActive" : "customLink"}
      as={NextLink}
      href={`/${page.path}`}
      onClick={() => {
        console.log("pathname !== `/${page.path}`", pathname !== `/${page.path}`)
        console.log("/${page.path}", `/${page.path}`)
        console.log("pathname`", pathname)
        if (pathname !== `/${page.path}`) {
          setIsLoading(true);
        } else {
          // console.log("onLand", onLand)
        }
        onLand && onLand();          
      }}
      isLoading={loadingTest ? loadingTest : isLoading}
      loadingText={children ? children : page.name}
      justifyContent={"center"}
      {...rest}
    >
      {children ? children : page.name}
    </Button>
  );
};
