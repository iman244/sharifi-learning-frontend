import { Flex, Icon, ListItem, Text, UnorderedList } from "@chakra-ui/react";
import React from "react";
import { digitsEnToFa } from "@persian-tools/persian-tools";
import { FaInstagram, FaTelegramPlane, FaWhatsapp } from "react-icons/fa";
import Link from "next/link";

export const Footer = () => {
  return (
    <Flex bgColor={"gray.200"} dir="rtl" padding={"64px"} as={"footer"}>
      <Flex
        flexDir={{
          base: "column",
          md: "row",
        }}
        justifyContent={"space-between"}
        gap={"64px"}
        maxW={"1000px"}
        w={"100%"}
      >
        <UnorderedList spacing={"12px"}>
          <ListItem>
            <Link href={"/"}>صفحه اصلی</Link>
          </ListItem>
          <ListItem>
            <Link href={"/store"}>فروشگاه</Link>
          </ListItem>
          {/* <ListItem>
            <a referrerPolicy='origin' target='_blank' href='https://trustseal.enamad.ir/?id=477250&Code=ghkB2kymoZ2PhumFkXXXGZ5Rw1nguSs3'>
              <img referrerPolicy='origin' src='https://trustseal.enamad.ir/logo.aspx?id=477250&Code=ghkB2kymoZ2PhumFkXXXGZ5Rw1nguSs3' alt='' style={{cursor: 'pointer'}} Code='ghkB2kymoZ2PhumFkXXXGZ5Rw1nguSs3' />
            </a>
          </ListItem> */}
        </UnorderedList>
        <UnorderedList spacing={"12px"}>
          <ListItem>
            <Flex gap={"12px"}>
              <Text>شماره تماس:</Text>
              <a  href="tel:+989155145350" style={{ color: "blue", direction: 'ltr' }}>
                {digitsEnToFa("+989155145350")}
              </a>
            </Flex>
          </ListItem>
          <ListItem>
            <Flex gap={"12px"}>
              <Text>ایمیل:</Text>
              <a href="mailto:shryfy74@gmail.com" style={{ color: "blue" }}>
                shryfy74@gmail.com
              </a>
            </Flex>
          </ListItem>
          <ListItem>
            <Flex gap={"12px"}>
              <Text>آدرس:</Text>
              <Text>مشهد، چهارراه دکترا</Text>
            </Flex>
          </ListItem>
        </UnorderedList>
        <Flex gap={"12px"} h={"fit-content"} flexDir={"column"}>
          <Text>فضای مجازی</Text>
          <Flex gap={"24px"}>
            <a href="https://www.instagram.com/sharifiielts" target="_blank">
              <Icon as={FaInstagram} boxSize={5} />
            </a>
            <a>
              <Icon as={FaTelegramPlane} boxSize={5} />
            </a>
            <a>
              <Icon as={FaWhatsapp} boxSize={5} />
            </a>
            <a>ایتا</a>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};
