import React, { useState } from "react";
import Form from "../infrastructure/Form";
import { uploadStorageData } from "../types/backend/storageAPI";
import axios from "axios";
import { useToast } from "@chakra-ui/react";
import { NewFileDto } from "../types/backend/dtos";
var mime = require("mime-types");

export const UploadStorageForm = ({
  children,
  setUploadProgress,
  onSettledF,
}: {
  children: any;
  onSettledF?: () => void;
  setUploadProgress: any;
}) => {
  const toast = useToast();
  const [file, setFile] = useState<File>();
  return (
    <Form
      url="/storage/upload"
      onSubmitF={(data: { fileName: string; file: File }): NewFileDto => {
        setFile(data.file);
        return {
          fileName: data.fileName,
          mimeType: mime.lookup(data.file.name),
        };
      }}
      onSettledF={onSettledF && onSettledF}
      case201={async (data: uploadStorageData) => {
        toast({
          status: "info",
          position: "bottom",
          duration: 3000,
          description: "آپلود شروع شد...",
        });
        const { url, fields } = data.PresignedPost;

        const form = new FormData();
        Object.entries(fields).forEach(([field, value]) => {
          form.append(field, value);
        });
        if (file) {
          form.append("file", file);
        } else {
          console.log("file was not there", file);
        }

        let urlpure = url.match(/(?<=https:\/\/)[\s\S]*/)![0];

        try {
          await axios.post(`https://${urlpure}`, form, {
            headers: { "Content-Type": "multipart/form-data" },
            onUploadProgress: function (progressEvent) {
              setUploadProgress(() => {
                if (progressEvent.total) {
                  return Math.round(
                    (progressEvent.loaded * 100) / progressEvent.total
                  );
                } else {
                  return 0;
                }
              });
            },
          });
          setUploadProgress(null);
          toast({
            status: "success",
            position: "bottom",
            duration: 3000,
            description: `آپلود فایل با موفقیت انجام شد.`,
          });
        } catch (error) {
          console.log("UploadStorageForm error", error);
          setUploadProgress(null);
          toast({
            status: "warning",
            position: "bottom",
            duration: 3000,
            description:
              "document was created, but upload was unsuccessful! please upload your file in storage page",
          });
        }
      }}
    >
      {children}
    </Form>
  );
};
