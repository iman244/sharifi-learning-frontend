import React, { useState } from "react";
import { SubmitButton } from "../infrastructure/FormComponents";
import Form from "../infrastructure/Form";
import { getStorageFile } from "../types/backend/storageAPI";
import { Icon } from "@chakra-ui/react";
import { FaCloudDownloadAlt } from "react-icons/fa";

export const DownloadFile = ({ fileName }: { fileName: string }) => {
  const loadingState = useState(false);
  return (
    <Form
      url={`storage/${fileName}`}
      loadingState={loadingState}
      case201={(data: getStorageFile) => {
        window.open(data.preSignedURL, "_blank");
      }}
    >
      <SubmitButton
        isLoading={loadingState[0]}
        icon={<Icon as={FaCloudDownloadAlt} boxSize={5} />}
      >
        فایل
      </SubmitButton>
    </Form>
  );
};
