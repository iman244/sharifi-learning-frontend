import usePost from "@/app/infrastructure/hooks/usePost";
import { frontIP } from "@/app/infrastructure/http-common";
import { simpleQuery } from "@/app/infrastructure/types/inf_frontTypes";
import { CartContext } from "@/app/logics/cart";
import { UserContext } from "@/app/logics/user";
import { product } from "@/app/types/backend/entities";
import { Button, Flex, Text } from "@chakra-ui/react";
import { addCommas, digitsEnToFa } from "@persian-tools/persian-tools";
import { AxiosResponse } from "axios";
import { usePathname, useRouter } from "next/navigation";
import React, { useContext } from "react";
import { UseQueryResult } from "react-query";

export const PaymentGate = () => {
  const { cart_items } = useContext(CartContext);
  const { user } = useContext(UserContext);
  const pathname = usePathname();
  const router = useRouter();

  const Purchase = usePost(
    `/store/purchase?front_callback_url=${frontIP}/payment`,
    {
      case200: (data) => {
        if (data.payment_gate_url) router.push(data.payment_gate_url);
      },
    }
  );

  return (
    <Flex
      dir="rtl"
      alignItems={"flex-end"}
      w={"100%"}
      flexDir={"column"}
      gap={"24px"}
    >
      <TotalPrice cart={cart_items} />
      <DiscountPrice cart={cart_items} />
      <Price cart={cart_items} />
      <Button
        isLoading={Purchase.isLoading}
        disabled={true}
        colorScheme="green"
        w={"100%"}
        onClick={() => {
          if (user) {
            Purchase.mutate({});
          } else {
            router.push(`/auth?next=${pathname}`);
          }
        }}
      >
        {user ? "پرداخت" : "برای پرداخت ابتدا باید وارد شوید"}
      </Button>
    </Flex>
  );
};



const TotalPrice = ({ cart }: { cart: product[] }) => {
  return (
    <Flex justifyContent={"space-between"} alignItems={"center"} w={"100%"}>
      <Text>مبلغ کل:</Text>
      <Flex gap={"6px"}>
        <Text>
          {digitsEnToFa(
            addCommas(
              cart.reduce((accumulator: number, currentValue: product) => {
                return accumulator + currentValue.price;
              }, 0)
            )
          )}
        </Text>
        <Text fontWeight={300} fontSize={"sm"}>
          تومان
        </Text>
      </Flex>
    </Flex>
  );
};

const DiscountPrice = ({ cart }: { cart: product[] }) => {
  return (
    <Flex justifyContent={"space-between"} alignItems={"center"} w={"100%"}>
      <Text>تخفیف:</Text>
      <Flex gap={"6px"}>
        <Text>
          {digitsEnToFa(
            addCommas(
              cart.reduce((accumulator: number, currentValue: product) => {
                return accumulator + currentValue.discount;
              }, 0)
            )
          )}
        </Text>
        <Text fontWeight={300} fontSize={"sm"}>
          تومان
        </Text>
      </Flex>
    </Flex>
  );
};

const Price = ({ cart }: { cart: product[] }) => {
  return (
    <Flex justifyContent={"space-between"} alignItems={"center"} w={"100%"}>
      <Text fontWeight={300} fontSize={"sm"}>
        مبلغ قابل پرداخت:
      </Text>
      <Flex gap={"6px"}>
        <Text>
          {digitsEnToFa(
            addCommas(
              cart.reduce((accumulator: number, currentValue: product) => {
                return (
                  accumulator + (currentValue.price - currentValue.discount)
                );
              }, 0)
            )
          )}
        </Text>
        <Text fontWeight={300} fontSize={"sm"}>
          تومان
        </Text>
      </Flex>
    </Flex>
  );
};
