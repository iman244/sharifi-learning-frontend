import { useContainer } from "@/app/infrastructure/hooks/useContainer";
import usePost from "@/app/infrastructure/hooks/usePost";
import { frontIP } from "@/app/infrastructure/http-common";
import { simpleQuery } from "@/app/infrastructure/types/inf_frontTypes";
import { CartContext } from "@/app/logics/cart";
import { UserContext } from "@/app/logics/user";
import { product } from "@/app/types/backend/entities";
import { Button, Flex, Text } from "@chakra-ui/react";
import { addCommas, digitsEnToFa } from "@persian-tools/persian-tools";
import { AxiosResponse } from "axios";
import { usePathname, useRouter } from "next/navigation";
import React, { useContext, useState } from "react";
import { UseQueryResult } from "react-query";

export const AnonymousPaymentGate = () => {
  const { offlineCart } = useContext(CartContext);
  const [productsList, setProductsList] = useState<product[]>([]);
  const pathname = usePathname();
  const router = useRouter();

  const cartProductsQuery = useContainer("store/products", {
    keys: ["products", offlineCart],
    case200: (data: product[]) => {
      let desiredData = data.filter((product) => {
        for (let index = 0; index < offlineCart.length; index++) {
          if (product.name === offlineCart[index]) {
            return true;
          }
        }
        return false;
      });
      setProductsList(desiredData);
      return desiredData;
    },
  });

  return (
    <Flex
      dir="rtl"
      alignItems={"flex-end"}
      w={"100%"}
      flexDir={"column"}
      gap={"24px"}
    >
      <TotalPrice cart={productsList} />
      <DiscountPrice cart={productsList} />
      <Price cart={productsList} />
      <Button
        disabled={true}
        colorScheme="green"
        w={"100%"}
        onClick={() => {
          router.push(`/auth?next=${pathname}`);
        }}
      >
        برای پرداخت ابتدا باید وارد شوید
      </Button>
    </Flex>
  );
};

// interface cartProductsQueryType {
//   simpleQuery: simpleQuery<any>;
//   query: UseQueryResult<AxiosResponse<any, any>, any>;
//   refreshGET: () => void;
// }

const TotalPrice = ({ cart }: { cart: product[] }) => {
  return (
    <Flex justifyContent={"space-between"} alignItems={"center"} w={"100%"}>
      <Text>مبلغ کل:</Text>
      <Flex gap={"6px"}>
        <Text>
          {digitsEnToFa(
            addCommas(
              cart.reduce((accumulator: number, currentValue: product) => {
                return accumulator + currentValue.price;
              }, 0)
            )
          )}
        </Text>
        <Text fontWeight={300} fontSize={"sm"}>
          تومان
        </Text>
      </Flex>
    </Flex>
  );
};

const DiscountPrice = ({ cart }: { cart: product[] }) => {
  return (
    <Flex justifyContent={"space-between"} alignItems={"center"} w={"100%"}>
      <Text>تخفیف:</Text>
      <Flex gap={"6px"}>
        <Text>
          {digitsEnToFa(
            addCommas(
              cart.reduce((accumulator: number, currentValue: product) => {
                return accumulator + currentValue.discount;
              }, 0)
            )
          )}
        </Text>
        <Text fontWeight={300} fontSize={"sm"}>
          تومان
        </Text>
      </Flex>
    </Flex>
  );
};

const Price = ({ cart }: { cart: product[] }) => {
  return (
    <Flex justifyContent={"space-between"} alignItems={"center"} w={"100%"}>
      <Text fontWeight={300} fontSize={"sm"}>
        مبلغ قابل پرداخت:
      </Text>
      <Flex gap={"6px"}>
        <Text>
          {digitsEnToFa(
            addCommas(
              cart.reduce((accumulator: number, currentValue: product) => {
                return (
                  accumulator + (currentValue.price - currentValue.discount)
                );
              }, 0)
            )
          )}
        </Text>
        <Text fontWeight={300} fontSize={"sm"}>
          تومان
        </Text>
      </Flex>
    </Flex>
  );
};
