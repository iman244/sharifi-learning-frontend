import { CustomPageLink } from "@/app/components/CustomPageLink";
import QueryWindow from "@/app/infrastructure/QueryWindow";
import { useContainer } from "@/app/infrastructure/hooks/useContainer";
import { CartContext } from "@/app/logics/cart";
import { product } from "@/app/types/backend/entities";
import { Box, Button, Flex, Text } from "@chakra-ui/react";
import { addCommas, digitsEnToFa } from "@persian-tools/persian-tools";
import Image from "next/image";
import NextLink from "next/link";
import React, { useContext, useEffect } from "react";

export const DisplayCartProduct = ({ product }: { product: product }) => {
  const { removeProductFormCart } = useContext(CartContext);

  return (
    <Flex
      dir="rtl"
      padding={"12px"}
      bg={"gray.100"}
      borderRadius={"12px"}
      w={"100%"}
    >
      <Flex dir="rtl" gap={"24px"}>
        <Box w={"100px"} height={"100px"} overflow={"hidden"}>
          <Image
            style={{ objectFit: "contain", height: "100px" }}
            alt={product.image.name}
            src={product.image.file}
            width={100}
            height={100}
          />
        </Box>
        <Flex flexDir={"column"} gap={"12px"} justifyContent={"center"}>
          <Flex alignItems={"center"} gap={"12px"}>
            <Text fontWeight={300} fontSize={"sm"}>
              محصول:
            </Text>
            <Text>{product.name}</Text>
          </Flex>
          <Flex alignItems={"center"} gap={"12px"}>
            <Text fontWeight={300} fontSize={"sm"}>
              قیمت:
            </Text>
            <Flex gap={"6px"}>
              <Text>
                {digitsEnToFa(addCommas(product.price - product.discount))}
              </Text>
              <Text fontWeight={300} fontSize={"sm"}>
                تومان
              </Text>
            </Flex>
          </Flex>
          <Flex justifyContent={"flex-end"} gap={"12px"}>
            <CustomPageLink
              page={{ path: `store/${product.id}`, name: "مشاهده محصول" }}
              size={"sm"}
              bgColor={"skyblue"}
              _hover={{
                bgColor: "skyblue",
              }}
            />
            <Button
              colorScheme="red"
              size={"sm"}
              onClick={() => {
                // @ts-ignore
                removeProductFormCart(product);
              }}
            >
              حذف
            </Button>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};
