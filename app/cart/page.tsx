"use client";
import { Button, Flex, Heading, Icon, Link, Text } from "@chakra-ui/react";
import React, { useContext, useEffect, useState } from "react";
import { LiaFileInvoiceDollarSolid } from "react-icons/lia";
import { DisplayCartProduct } from "./components/DisplayCartProduct";
import { PaymentGate } from "./components/PaymentGate";
import { CartContext } from "../logics/cart";
import NextLink from "next/link";
import { CustomPageLink } from "../components/CustomPageLink";
import { UserContext } from "../logics/user";
import { AnonymousUserDisplayCartProduct } from "./components/AnonymousUserDisplayCartProduct";
import { AnonymousPaymentGate } from "./components/AnonymousPaymentGate";

export default function CartPage() {
  const { cart_items, offlineCart } = useContext(CartContext);
  const { user, userGetLoading } = useContext(UserContext);

  return (
    <Flex justifyContent={"center"} alignItems={"center"}>
      <Flex
        alignItems={"center"}
        justifyContent={"center"}
        boxShadow={"lg"}
        flexDir={"column"}
        gap={"24px"}
        padding={"24px"}
        bgColor={"white"}
      >
        <Flex gap={"12px"} alignItems={"center"} justifyContent={"center"}>
          <Heading>سبد خرید</Heading>
          <Icon
            as={LiaFileInvoiceDollarSolid}
            boxSize={8}
            color={"green.300"}
          />
        </Flex>
        <Flex flexDir={"column"} gap={"12px"} w={"100%"}>
          {!userGetLoading && user
            ? cart_items.length === 0 && <EmptyCartMessage />
            : offlineCart.length === 0 && <EmptyCartMessage />}
          {!userGetLoading && user
            ? cart_items.map((product, index) => (
                <DisplayCartProduct key={index} product={product} />
              ))
            : offlineCart.map((productName, index) => (
                <AnonymousUserDisplayCartProduct key={index} productName={productName} />
              ))}
        </Flex>
        {cart_items.length !== 0 && (
          <CustomPageLink
            w={"100%"}
            page={{
              path: "store",
            }}
          >
            ادامه خرید
          </CustomPageLink>
        )}
        {!userGetLoading && user
          ? cart_items.length !== 0 && <PaymentGate />
          : offlineCart.length !== 0 && <AnonymousPaymentGate />}
      </Flex>
    </Flex>
  );
}

const EmptyCartMessage = () => {
  return (
    <Text>
      سبد خرید شما خالی است! برای خرید{" "}
      <Link as={NextLink} href="/store" color={"blue"} textDecor={"underline"}>
        اینجا
      </Link>{" "}
      کلیک کنید
    </Text>
  );
};
