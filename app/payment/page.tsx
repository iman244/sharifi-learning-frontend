"use client";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import React, { useContext, useEffect, useState } from "react";
import { useContainer } from "../infrastructure/hooks/useContainer";
import {
  CircularProgress,
  Divider,
  Flex,
  Heading,
  Text,
} from "@chakra-ui/react";
import QueryWindow from "../infrastructure/QueryWindow";
import useGet from "../infrastructure/hooks/useGet";
import { payment } from "../types/backend/entities";
import { addCommas, digitsEnToFa } from "@persian-tools/persian-tools";
import { ProductCard } from "../store/components/ProductCard";
import { CartContext } from "../logics/cart";
import { PaymentGate } from "../cart/components/PaymentGate";
import { SmLoading } from "../infrastructure/SmLoading";
import CartPage from "../cart/page";
import { CustomPageLink } from "../components/CustomPageLink";

export default function Page() {
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const router = useRouter();
  const [payment, setPayment] = useState<payment>();

  const GET = useGet(
    `store/verify-payment?paymentId=${searchParams.get("paymentId")}`,
    ["verify-payment"],
    {
      case200: (data: payment) => {
        setPayment(data);
        return data;
      },
      caseError(err: any) {
        if (err.response.status == 401) {
          router.push(
            `/auth?next=${pathname}?paymentId=${searchParams.get("paymentId")}`
          );
        }
      },
    }
  );


  return (
    <Flex width={"100%"} justifyContent={"center"} dir="rtl" flexDir={"column"}>
   
      { GET.status === "loading" ? (
        <SmLoading />
      ) : (
        <>
          {/* @ts-ignore */}
          {GET.status === "error" && GET.error.response?.status == 401 && (
            <Flex>
              <Text>انتقال به صفحه ورود ...</Text>
            </Flex>
          )}
          {GET.status === "success" && payment && payment.status ? (
            <SuccesPayment payment={payment} />
          ) : (
            <FailurePayment />
          )}
        </>
      )}
    </Flex>
  );
}

const FailurePayment = () => {
  return (
    <Flex
      flexDir={"column"}
      gap={"36px"}
      justifyContent={"center"}
      alignItems={"center"}
    >
      <Text color={"red"}>
        پرداخت شما موفقیت آمیز نبوده است، لطفاً مجدداً تلاش فرمایید
      </Text>
      <PaymentListLink />
      <CartPage />
    </Flex>
  );
};

const SuccesPayment = ({ payment }: { payment: payment }) => {
  return (
    <Flex
      flexDir={"column"}
      gap={"36px"}
      justifyContent={"center"}
      alignItems={"center"}
    >
      <Text fontWeight={500}>
        <Text color={"green"}>پرداخت شما موفقیت آمیز بود</Text>
      </Text>
      <PaymentListLink />

      <Flex gap={"24px"} justifyContent={"center"}>
        <Text>مبلغ:</Text>
        <Flex gap={"8px"}>
          <Text>{digitsEnToFa(addCommas(payment.amount))}</Text>
          <Text>تومان</Text>
        </Flex>
      </Flex>
      <Divider />
      <Heading>محصولات خریداری شده</Heading>
      <Flex gap={"24px"}>
        {payment?.products.map((product, index) => {
          return <ProductCard key={index} product={product} />;
        })}
      </Flex>
    </Flex>
  );
};

const PaymentListLink = () => {
  return (
    <CustomPageLink page={{ path: "profile/payments" }}>
      لیست پرداخت‌های شما
    </CustomPageLink>
  );
};
