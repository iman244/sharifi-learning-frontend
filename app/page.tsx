"use client";
import { Flex, Heading } from "@chakra-ui/react";
import { useContainer } from "./infrastructure/hooks/useContainer";
import { category } from "./types/backend/entities";
import QueryWindow from "./infrastructure/QueryWindow";
import { DisplayCategory } from "./components/DisplayCategory";
import { Auth } from "./auth/Auth";
import Head from "next/head";
import { useContext } from "react";
import { UserContext } from "./logics/user";

export default function Home() {
  const categories = useContainer("store/category", {
    keys: ["categories"],
  });

  const {user , userGetLoading} = useContext(UserContext)

  return (
    <Flex flexDir={"column"} gap={"48px"} dir="rtl" >
      {!userGetLoading && !user && <Flex w={'100%'} justifyContent={'center'}><Auth /></Flex>}

      <QueryWindow query={categories.query}>
          <Heading textAlign={'center'}>دسته بندی محصولات</Heading>
        <Flex justifyContent={"center"} gap={"24px"} wrap={"wrap"}>
          {categories.simpleQuery?.data?.map((category: category, index: number) => (
            <DisplayCategory key={index} category={category} />
          ))}
        </Flex>
      </QueryWindow>
    </Flex>
  );
}
