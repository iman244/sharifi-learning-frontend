"use client";

import { product } from "@/app/types/backend/entities";
import { CircularProgress, Flex, Text } from "@chakra-ui/react";
import { useSearchParams } from "next/navigation";
import React, { useEffect, useState } from "react";
import { ProductQuery } from "./components/ProductQuery";
import { CategoryQuery } from "./components/CategoryQuery";
import { DisplayActiveQueries } from "./components/DisplayActiveQueries";
import { ProductCard } from "./components/ProductCard";
import useGet from "../infrastructure/hooks/useGet";
import { SmLoading } from "../infrastructure/SmLoading";

export default function CategoryProducts() {
  const searchParams = useSearchParams();
  const [categorySelected, setCategorySelected] = useState<any>();
  const [qcategoryName, setQCategoryName] = useState(
    searchParams.get("categoryName")
  );
  const [qproductName, setQProductName] = useState("");
  const GET = useGet(
    `store/products?category=${
      qcategoryName ? qcategoryName : ""
    }&name=${qproductName}`,
    ["products", qcategoryName, qproductName]
  );


  useEffect(()=>{
console.log("GET",GET)
  },[GET])

  return (
    <Flex flexDir={"column"} gap={"36px"} dir="rtl">
      <Flex gap={"24px"} flexDir={{ base: "column", sm: "row" }}>
        <ProductQuery
          qproductName={qproductName}
          setQProductName={setQProductName}
        />
        <CategoryQuery
          categorySelected={categorySelected}
          setCategorySelected={setCategorySelected}
          defaultValue={searchParams.get("categoryName")}
          setQCategoryName={setQCategoryName}
        />
      </Flex>
      <DisplayActiveQueries
        qcategoryName={qcategoryName}
        qproductName={qproductName}
        setQCategoryName={setQCategoryName}
        setCategorySelected={setCategorySelected}
        setQProductName={setQProductName}
      />

      <Flex dir="rtl" flexWrap={"wrap"} gap={"36px"} justifyContent={'center'}>
        {GET.status !== "success" && <SmLoading />
        }
        {GET.status === "success" &&
          GET.data.data.length === 0 &&
          !!qproductName && (
            <Text>
              محصولی با نام {qproductName} در حال حاضر وجود ندارد :&#40;
            </Text>
          )}
        {GET.status === "success" &&
          GET.data.data.length === 0 &&
          !qproductName && <Text>محصولی در حال حاضر وجود ندارد :&#40;</Text>}
        {GET.status === "success" &&
          GET.data.data.map((product: product, index: number) => {
            return <ProductCard key={index} product={product} />;
          })}
      </Flex>
    </Flex>
  );
}
