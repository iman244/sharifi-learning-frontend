"use client";
import QueryWindow from "@/app/infrastructure/QueryWindow";
import { useContainer } from "@/app/infrastructure/hooks/useContainer";
import { Box, Flex, Heading, Text } from "@chakra-ui/react";
import React, { useContext, useEffect } from "react";
import parse from "html-react-parser";
import { product } from "@/app/types/backend/entities";
import Image from "next/image";
import { CartButton, PriceInfo } from "../components/ProductCard";
import { UserContext } from "@/app/logics/user";
import { CustomPageLink } from "@/app/components/CustomPageLink";

export default function Page({ params }: { params: { pk_product: string } }) {
  const productQuery = useContainer(`store/products/${params.pk_product}`, {
    enabled: !!params.pk_product,
    keys: [params.pk_product],
  });

  return (
    <QueryWindow query={productQuery.query}>
      {productQuery.simpleQuery?.data && (
        <Flex flexDir={"column"} dir="rtl" gap={"24px"}>
          <ProductInfo product={productQuery.simpleQuery.data} />
          <Box w="100%" dir="ltr" padding={"24px"}>
            {parse(productQuery.simpleQuery.data.content)}
          </Box>
        </Flex>
      )}
    </QueryWindow>
  );
}

const ProductInfo = ({ product }: { product: product }) => {
  const { haveProduct } = useContext(UserContext);

  return (
    <Flex gap={"12px"} alignItems={"center"} height={'fit-content'}>
      <Image
        src={product.image.file}
        width={120}
        height={300}
        alt={product.image.name}
      />
      <Flex flexDir={"column"} gap={'18px'}>
        <Heading>{product.name}</Heading>
        {haveProduct && !haveProduct(product.name) && (
          <PriceInfo product={product} />
        )}
        {haveProduct && haveProduct(product.name) ? (
          <CustomPageLink page={{ path: "profile" }}>
            دانلود در صفحه پروفایل
          </CustomPageLink>
        ) : (
          <CartButton product={product} />
        )}
      </Flex>
    </Flex>
  );
};
