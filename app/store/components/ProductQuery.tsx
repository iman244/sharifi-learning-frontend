import {
  Button,
  Flex,
  Icon,
  Input,
  InputGroup,
  InputLeftElement,
  Select,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { FaSearch } from "react-icons/fa";
import { CategoryQuery } from "./CategoryQuery";

export const ProductQuery = ({
  qproductName,
  setQProductName,
}: {
  qproductName: string;
  setQProductName: React.Dispatch<React.SetStateAction<string>>;
}) => {
  const [productName, setProductName] = useState("");

  return (
    <InputGroup w={"200px"} bgColor={'white'}>
      <Input
        paddingLeft={"50px"}
        paddingRight={"10px"}
        placeholder="جستجو در محصولات"
        value={qproductName}
        onChange={(e) => setQProductName(e.target.value)}
      />
      <InputLeftElement w={"50px"}>
        <Button
          size={"sm"}
          onClick={() => {
            // setProductName("");
            // setQProductName(productName);
          }}
        >
          <Icon as={FaSearch} />
        </Button>
      </InputLeftElement>
    </InputGroup>
  );
};
