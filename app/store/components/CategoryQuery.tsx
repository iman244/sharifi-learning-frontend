import { useContainer } from "@/app/infrastructure/hooks/useContainer";
import { category } from "@/app/types/backend/entities";
import Select, { StylesConfig } from "react-select";
import { useState } from "react";
import { Flex, Text } from "@chakra-ui/react";

export const CategoryQuery = ({
  defaultValue,
  setQCategoryName,
  categorySelected,
  setCategorySelected,
}: {
  defaultValue: string | null;
  setQCategoryName: React.Dispatch<React.SetStateAction<string | null>>;
  categorySelected: any;
  setCategorySelected: React.Dispatch<React.SetStateAction<any>>;
}) => {
  const [categoryOptions, setCategoryOptions] = useState([]);

  const categories = useContainer("store/category", {
    keys: ["categories"],
    case200(data) {
      setCategoryOptions(
        data.map((category: category) => ({
          label: category.name,
          value: category.name,
        }))
      );
      return data;
    },
  });

  return (
    <Flex gap={"12px"} alignItems={"center"}>
      {categoryOptions.length > 0 && (
        <>
          <Text display={{ base: "none", sm: "block" }}>دسته‌بندی:</Text>
          <Select
            styles={selectStyles}
            placeholder={"دسته‌بندی"}
            isClearable
            defaultValue={() => {
              if (defaultValue !== null) {
                return categoryOptions.filter(
                  (v: any) => v.value === defaultValue
                );
              }
            }}
            options={categoryOptions}
            value={categorySelected}
            onChange={(option) => {
              if (option) {
                setCategorySelected(option);
              } else {
                setCategorySelected(null);
              }
              setQCategoryName(option ? option.value : "");
            }}
          />
        </>
      )}
    </Flex>
  );
};

const selectStyles: StylesConfig<any> = {
  control: (styles) => ({
    ...styles,
    width: "200px",
    borderColor: "#E2E8F0",
    borderRadius: "0.375rem",
    ":hover": {
      borderColor: "#CBD5E0",
    },
  }),
};
