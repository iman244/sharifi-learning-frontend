import React, { MouseEvent, useContext, useEffect, useState } from "react";

import { Button, Flex, Heading, Icon, Text } from "@chakra-ui/react";
import Image from "next/image";
import { addCommas, digitsEnToFa } from "@persian-tools/persian-tools";
import { FaCartPlus } from "react-icons/fa";
import { BsCartDashFill, BsFillCartCheckFill } from "react-icons/bs";
import { useRouter } from "next/navigation";
import { product } from "@/app/types/backend/entities";
import { CardLoading } from "@/app/components/CardLoading";
import { CartContext } from "@/app/logics/cart";
import { UserContext } from "@/app/logics/user";
import { CustomPageLink } from "@/app/components/CustomPageLink";

export const ProductCard = ({ product }: { product: product }) => {
  const router = useRouter();
  const [isLoadingProductPage, setIsLoadingProductPage] = useState(false);

  const { haveProduct } = useContext(UserContext);

  return (
    <Flex
      w={"300px"}
      alignItems={"center"}
      flexDir={"column"}
      borderRadius={"16px"}
      overflow={"hidden"}
      boxShadow={"md"}
      bgColor={"white"}
      _hover={{ boxShadow: "2xl" }}
      transition={"all 0.3s linear"}
      position={"relative"}
    >
      {isLoadingProductPage && <CardLoading />}
      <Flex w={"300px"} height={"300px"} bgColor={"gray.200"}>
        <Image
          style={{ objectFit: "contain" }}
          src={product.image.file}
          alt={product.image.name}
          width={300}
          height={300}
        />
      </Flex>
      <Flex
        alignItems={"center"}
        gap={"12px"}
        justifyContent={'center'}
        flexDir={"column"}
        padding={"12px"}
      >
        <Heading
          onClick={() => {
            setIsLoadingProductPage(true);
            router.push(`store/${product.id}`);
          }}
          cursor={"pointer"}
          _hover={{
            textDecor: "underline",
          }}
        >
          {product.name}
        </Heading>
        {/* <Text variant={"description"}>
          {product.content.length >= 15
            ? `${product.content.slice(0, 14)}...`
            : product.content}
        </Text> */}

        {haveProduct && !haveProduct(product.name) && (
          <PriceInfo product={product} />
        )}
        {haveProduct && haveProduct(product.name) ? (
          <CustomPageLink style={{margin: '16px'}} page={{ path: "profile" }}>
            دانلود در صفحه پروفایل
          </CustomPageLink>
        ) : (
          <CartButton product={product} />
        )}
      </Flex>
    </Flex>
  );
};

export const CartButton = ({ product }: { product: product }) => {
  const router = useRouter();
  const [isLoadingCartPage, setIsLoadingCartPage] = useState(false);
  const [isLoadingCartLogics, setIsLoadingCartLogics] = useState(false);

  const { isInCart, addProductToCart, removeProductFormCart } =
    useContext(CartContext);

  return (
    <Flex gap={"12px"}>
      {isInCart && isInCart(product.name) && (
        <Button
          bgColor={"yellow.400"}
          _hover={{
            bgColor: "yellow.500",
          }}
          onClick={() => {
            if (removeProductFormCart) {
              setIsLoadingCartLogics(true);
              const status = removeProductFormCart(product);
              setIsLoadingCartLogics(status.loading);
            }
          }}
        >
          <Icon as={BsCartDashFill} />
        </Button>
      )}
      <Button
        isLoading={isLoadingCartPage || isLoadingCartLogics}
        colorScheme={product.price === 0 ? "blue" : "green" }
        leftIcon={
          <Icon
            as={
              isInCart && isInCart(product.name)
                ? BsFillCartCheckFill
                : FaCartPlus
            }
          />
        }
        onClick={(e) => {
          e.stopPropagation();
          if (isInCart && isInCart(product.name)) {
            setIsLoadingCartPage(true);
            router.push("/cart");
          } else {
            if (addProductToCart) {
              setIsLoadingCartLogics(true);
              const status = addProductToCart(product);
              setIsLoadingCartLogics(status.loading);
            }
          }
        }}
      >
        {isInCart && isInCart(product.name)
          ? "مشاهده سبد خرید"
          : product.price === 0 ? "افزودن حساب کاربری" : "افزودن به سبد خرید"}
      </Button>
    </Flex>
  );
};

export const PriceInfo = ({ product }: { product: product }) => {
  return (
    <Flex gap={"12px"}>
      {product.price !== 0 && <Text>قیمت:</Text>}
      <Text
        textDecor={!product.discount ? "none" : "solid line-through #718096"}
      >
        {product.price !== 0 && `${digitsEnToFa(addCommas(product.price))} تومان`}
        {product.price === 0 && <Text fontWeight={'600'} color={'blue.500'}>رایگان</Text>}
      </Text>
      {product.discount !== 0 && (
        <Text>
          {digitsEnToFa(addCommas(product.price - product.discount))} تومان
        </Text>
      )}
    </Flex>
  );
};
