import { Button, Flex, Icon, Text } from "@chakra-ui/react";
import React from "react";
import { IoClose } from "react-icons/io5";

export const DisplayActiveQueries = ({
  qcategoryName,
  setQCategoryName,
  qproductName,
  setQProductName,
  setCategorySelected,
}: {
  qcategoryName: string | null;
  setQCategoryName: React.Dispatch<React.SetStateAction<string | null>>;
  qproductName: string;
  setQProductName: React.Dispatch<React.SetStateAction<string>>;
  setCategorySelected: React.Dispatch<React.SetStateAction<any>>;
}) => {
  return (
    <Flex gap={"12px"} alignItems={"center"}>
      {(qproductName || qcategoryName) && <Text>فیلتر:</Text>}
      {qproductName && (
        <Flex
          alignItems={"center"}
          bgColor={"white"}
          gap={"12px"}
          // border={"1px solid #E2E8F0"}
          w={"fit-content"}
          paddingLeft={"16px"}
          borderRadius={"8px"}
          border={"1px solid #CBD5E0"}
        >
          <Button
            bgColor={"gray.300"}
            _hover={{ bgColor: "gray.400", borderColor: "#A0AEC0" }}
            borderLeftRadius={0}
            size={"sm"}
            onClick={() => {
              setQProductName("");
            }}
          >
            <Icon as={IoClose} />
          </Button>
          <Text>{qproductName}</Text>
        </Flex>
      )}
      {qcategoryName && (
        <Flex
          bgColor={"white"}
          alignItems={"center"}
          gap={"12px"}
          w={"fit-content"}
          paddingLeft={"16px"}
          borderRadius={"8px"}
          border={"1px solid #CBD5E0"}
        >
          <Button
            bgColor={"gray.300"}
            _hover={{ bgColor: "gray.400", borderColor: "#A0AEC0" }}
            borderLeftRadius={0}
            size={"sm"}
            onClick={() => {
              setQCategoryName("");
              setCategorySelected(null);
            }}
          >
            <Icon as={IoClose} />
          </Button>
          <Flex gap={"8px"} alignItems={"center"} justifyContent={"center"}>
            <Text>دسته‌بندی</Text>
            <Text>{qcategoryName}</Text>
          </Flex>
        </Flex>
      )}
    </Flex>
  );
};
