import { Flex, Spinner } from "@chakra-ui/react";
import React from "react";

export const SmLoading = () => {
  return (
    <Flex
      w={"100%"}
      height={"100%"}
      alignItems={"center"}
      justifyContent={"center"}
    >
      <Flex
        w={"35px"}
        height={"35px"}
        alignItems={"center"}
        justifyContent={"center"}
        bg={"gray.100"}
        borderRadius={"8px"}
      >
        <Spinner size={"sm"} />
      </Flex>
    </Flex>
  );
};
