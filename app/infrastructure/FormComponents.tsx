"use client";
import {
  Button,
  Flex,
  Spinner,
  Input,
  Text,
  Textarea,
  Checkbox,
  Icon,
} from "@chakra-ui/react";
import { Controller, useFormContext, useFormState } from "react-hook-form";
import {
  CompilicatedCreatableSelectProps,
  CompilicatedInputProps,
  CompilicatedSelectProps,
  FormInputUIProps,
  SubmitButtonProps,
} from "./types/inf_frontTypes";
import Select, { StylesConfig } from "react-select";
import CreatableSelect from "react-select/creatable";
import Form from "./Form";
import { useState } from "react";
import { Placeholder } from "react-select/animated";
import { MdDelete } from "react-icons/md";

export const ComplicatedInput = ({
  register,
  name,
  placeholder,
  rules,
  type = "text",
  w = "216px",
  ...rest
}: CompilicatedInputProps) => {
  const { control } = useFormContext();
  const { errors } = useFormState();
  return (
    <Flex flexDir={"column"} gap={"4px"} w={w}>
      <Text height={"24px"} fontSize={"0.8em"} color={"red.300"}>
        {errors[name] ? `${errors[name]?.message}` : ""}
      </Text>
      <Controller
        control={control}
        name={name}
        rules={rules && rules}
        render={({ field: { value, onChange, ...field } }: any) => {
          if (type === "file") {
            return (
              <FormInputUI
                register={register}
                placeholder={placeholder}
                type={type}
                value={value?.fileName}
                onChange={(event: any) => {
                  onChange(event.target.files[0]);
                }}
                {...field}
                {...rest}
              />
            );
          }
          return (
            <FormInputUI
              register={register}
              placeholder={placeholder}
              type={type}
              value={value}
              onChange={onChange}
              {...field}
              {...rest}
            />
          );
        }}
      />
    </Flex>
  );
};

export const ComplicatedSelectInput = ({
  register,
  name,
  placeholder,
  options,
  rules,
  isMulti = false,
  selectStyles,
  isClearable = true,
  defaultValue = null,
  ...rest
}: CompilicatedSelectProps) => {
  const { control } = useFormContext();
  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      render={({ field }: any) => {
        return (
          <Select
            register={register}
            placeholder={placeholder}
            options={options}
            styles={selectStyles}
            isClearable={isClearable}
            {...field}
            isMulti={isMulti}
          />
        );
      }}
    />
  );
};

export const ComplicatedCreatableSelectInput = ({
  register,
  name,
  placeholder,
  rules,
  options,
  selectStyles,
  isClearable = true,
  isDisabled = false,
  onCreateOption,
  defaultValue = null,
}: CompilicatedCreatableSelectProps) => {
  const { control } = useFormContext();

  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      render={({ field }: any) => {
        return (
          <CreatableSelect
            isDisabled={isDisabled}
            register={register}
            placeholder={placeholder}
            options={options}
            styles={selectStyles}
            isClearable={isClearable}
            onCreateOption={onCreateOption}
            {...field}
          />
        );
      }}
    />
  );
};

export const SubmitButton = ({
  children = "ارسال",
  variant,
  isLoading,
  loadingTest = false,
  simpleLoading = false,
  icon = undefined,
  ...rest
}: SubmitButtonProps) => {
  let spinnerColor =
    variant === "delete-btn" ? "red.400" : "rgba(200,200,200,1)";

  const showChildren =
    isLoading || loadingTest ? (simpleLoading ? false : true) : true;
  return (
    <Button type="submit" variant={variant} {...rest} dir="rtl">
      <Flex
        alignItems={"center"}
        justifyContent={"center"}
        w={"fit-content"}
        textAlign={"center"}
        gap={"12px"}
      >
        {loadingTest ? (
          <Spinner color={spinnerColor} size="xs" />
        ) : isLoading ? (
          <Spinner color={spinnerColor} size="xs" />
        ) : (
          icon && icon
        )}
        {showChildren && children}
      </Flex>
    </Button>
  );
};

export const FormInputUI = ({ type, ...props }: FormInputUIProps) => {
  if (type === "textarea") {
    return <Textarea {...props} autoComplete={"off"} />;
  } else if (type === "checkbox") {
    return (
      <Checkbox height={"40px"} {...props}>
        {props.placeholder}
      </Checkbox>
    );
  } else {
    return (
      <Input
        type={type}
        {...props}
        // variant={"borderless"}
        autoComplete={"off"}
        
      />
    );
  }
};

export const DeleteBtn = ({
  url,
  onSubmitF,
  onSettledF,
  btnText = "حذف",
}: {
  url: string;
  onSubmitF?: () => void;
  onSettledF?: () => void;
  btnText?: string;
}) => {
  const loadingState = useState(false);

  return (
    <Form
      url={url}
      method="delete"
      loadingState={loadingState}
      onSubmitF={onSubmitF}
      onSettledF={onSettledF && onSettledF}
    >
      <SubmitButton
        variant="delete-btn"
        isLoading={loadingState[0]}
        icon={<Icon as={MdDelete} boxSize={5} />}
      >
        {btnText}
      </SubmitButton>
    </Form>
  );
};
