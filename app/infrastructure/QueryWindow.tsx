import React from "react";
import { CircularProgress, Flex, Spinner } from "@chakra-ui/react";
import { QueryStatus } from "react-query";
import { query } from "./types/inf_frontTypes";

const QueryWindow = ({
  query,
  children,
  loadingTest = false,
  LoadingUI = () => (
    <Flex
      justifyContent={"center"}
      alignItems={"center"}
      w={"100%"}
      height={"100%"}
      flex={1}
    >
      <Flex
        bgColor={"gray.100"}
        height={"35px"}
        borderRadius={"8px"}
        w={"35px"}
        alignItems={"center"}
        justifyContent={"center"}
      >
        <Spinner size={"sm"} />
      </Flex>
    </Flex>
  ),
  ErrorUI = ({ query }) => (
    <Flex
      padding={"24px"}
      bgColor={"red.100"}
      w={"fit-content"}
      h={"fit-content"}
      color={"red.500"}
    >
      {query!.error!.code}
    </Flex>
  ),
}: {
  query: query;
  loadingTest?: boolean;
  LoadingUI?: () => JSX.Element;
  ErrorUI?: ({ query }: { query: query }) => JSX.Element;
  children?: any;
}) => {
  const windowStatus = !loadingTest
    ? query
      ? query.status
      : "loading"
    : "loading";
  const window = (status: QueryStatus) => {
    if (query) {
      switch (status) {
        case "error":
          return <ErrorUI query={query} />;
        case "idle":
          return <LoadingUI />;
        case "loading":
          return <LoadingUI />;
        case "success":
          return children;
        default:
          return "default";
      }
    } else return "query is not defined";
  };

  return <>{window(windowStatus)}</>;
};

export default QueryWindow;
