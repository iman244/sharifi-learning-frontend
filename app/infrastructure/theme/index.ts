import { extendTheme } from "@chakra-ui/react";
import Button from "./components/Button";
import GridItem from "./components/GridItem";
import Text from "./components/Text";
import Heading from "./components/Heading";
import Input from "./components/Input";
import Th from "./components/Th";
import Td from "./components/Td";

const theme = extendTheme({
  components: {
    // Th, doesnt work, i dont know why
    // Td,
    Input,
    Text,
    Heading,
    GridItem,
    Button,
  },
});
export default theme;
