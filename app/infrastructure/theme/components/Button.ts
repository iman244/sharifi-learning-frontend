import { defineStyleConfig, theme } from "@chakra-ui/react";

const Button = defineStyleConfig({
  variants: {
    customLink: (props) => ({
      ...theme.components.Button.variants?.ghost(props),
      justifyContent: "flex-start",
      fontWeight: "normal",
      border: "1px solid #A0AEC0",
      bgColor: "white",
    }),
    customLinkActive: (props) => ({
      ...theme.components.Button.variants?.solid(props),
      bgColor: "gray.300",
      justifyContent: "flex-start",
    }),
    "delete-btn": {
      bgColor: "red.100",
      _hover: { bgColor: "red.200" },
    },
  },
});

export default Button;
