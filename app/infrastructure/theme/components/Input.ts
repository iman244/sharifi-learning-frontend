import { defineStyleConfig } from "@chakra-ui/react";

const Input = defineStyleConfig({
  variants: {
    borderless: (props) => ({
      bgColor: "white",
      border: "none",
    }),
  },
});

export default Input;
