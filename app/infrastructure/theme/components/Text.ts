import { defineStyleConfig } from "@chakra-ui/react";

const Text = defineStyleConfig({
  variants: {
    gridHeader: (props) => ({
      borderBottom: "1px solid",
      padding: "12px",
      borderColor: "gray.300",
      w: "100%",
      textAlign: "center",
    }),
    description: (props) => ({
      color: "gray.400",
      fontSize: "sm",
    }),
  },
});

export default Text;
