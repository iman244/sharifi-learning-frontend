import { defineStyleConfig } from "@chakra-ui/react";

const Heading = defineStyleConfig({
  defaultProps: {
    size: "md",
  },
});

export default Heading;
