import { ComponentStyleConfig, defineStyleConfig } from "@chakra-ui/react";

const Th: ComponentStyleConfig = {
  baseStyle: {
    textAlign: "center",
  },
};
export default Th;
