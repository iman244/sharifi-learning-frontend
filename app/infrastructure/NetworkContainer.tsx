import React, { useEffect } from "react";
import { useContainer } from "./hooks/useContainer";
import {
  NetworkContainerInput,
  query,
  simpleQuery,
} from "./types/inf_frontTypes";

export const NetworkContainer = ({
  url,
  case200,
  log = false,
  render,
}: {
  url: string;
  case200?: (data: any) => any;
  log?: boolean;
  render: (NetworkContainerInput: NetworkContainerInput) => React.JSX.Element;
}) => {
  const { query, refreshGET, simpleQuery } = useContainer(url, {
    keys: [url],
    case200: (data) => {
      if (!!case200) {
        return case200(data);
      }
      return data;
    },
  });

  useEffect(() => {
    log && console.log(`${url} simpleQuery`, simpleQuery);
  }, [simpleQuery]);
  return render({ query, refreshGET, simpleQuery, url });
};
