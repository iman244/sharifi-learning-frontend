"use client";
import React, { useContext, useEffect, useState } from "react";
import { user } from "../types/backend/entities";
import { useContainer } from "./hooks/useContainer";
import { UserContext } from "../logics/user";
import { Flex, Spinner } from "@chakra-ui/react";
import { SmLoading } from "./SmLoading";

export const Authorization = ({
  children,
  roles,
  notValid,
  log = false,
  id,
  render,
}: {
  children?: any;
  notValid?: any;
  roles?: string[];
  log?: boolean;
  id?: string;
  render: (user: user) => React.JSX.Element;
}) => {
  const { user, userGetLoading } = useContext(UserContext);
  log && console.log("authorization user", user);

  if (userGetLoading) {
    return <SmLoading />
  }

  if (!!user) {
    log && console.log("authorization user", id, user);

    return render(user);
  }
  if (!!notValid) {
    log && console.log("we are in not valid condition");
    return notValid;
  }
};
