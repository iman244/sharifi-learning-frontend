"use client";
import { Box } from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { FormProvider, useFormContext } from "react-hook-form";
import { FormLayoutProps, FormProps } from "./types/restTypes";
import usePost from "./hooks/usePost";
import { InternalFormLayout } from "./InternalFormLayout";
import { useEffect } from "react";

const valueHandling = (value: any) => {
  if (!Number.isNaN(Number(value))) {
    return Number(value);
  }
  return value;
};

export default function Form({
  method = "post",
  ContentType = "application/json",
  loadingState,
  defaultValues,
  children,
  url,
  onSettledF,
  onSubmitF,
  case200,
  case201,
  case401,
  caseError,
  selectHandler = false,
  log = false,
  ...rest
}: FormProps) {
  const methods = useForm({ defaultValues });
  const { handleSubmit } = methods;
  const onSubmit = (data: any) => {
    // things that are handling now,
    // 1. loading
    // 2. file sending,
    // 3. react-select,
    // 3. number,

    loadingState && loadingState[1](true);
    log && console.log(`url ${method} onSubmit data`, data);
    var d = data;
    if (onSubmitF) {
      d = onSubmitF(data);
    } else if (selectHandler) {
      Object.entries(d).forEach(([field, value]: any, index) => {
        if (field === "password" || field === "newPassword") {
          d[field] = `${valueHandling(d[field])}`;
        } else if (value === undefined) {
          // I add this if for checkbox that is unchecked
          d[field] = false;
        } else if (typeof value !== "string") {
          if (Array.isArray(d[field])) {
            d[field] = d[field].map((item: { label: string; value: any }) =>
              valueHandling(item.value)
            );
          } else {
            d[field].value
              ? (d[field] = valueHandling(d[field].value))
              : valueHandling(value);
          }
        } else {
          d[field] = valueHandling(d[field]);
        }
      });
    }

    if (ContentType === "application/json") {
      POST.mutate(d);
    } else if (ContentType === "application/json") {
      let form = new FormData();
      Object.entries(d).forEach(([field, value]: any, index) => {
        if (field === "file") {
          form.append(field, value);
        } else {
          form.append(field, value);
        }
      });

      POST.mutate(form);
    }
  };
  const onError = (data: any) => log && console.log("onError data",data);
  const POST = usePost(url, {
    ContentType,
    loadingState,
    onSettledF,
    case200,
    case201,
    case401,
    caseError,
    method,
  });

  return (
    <FormProvider {...methods}>
      <Box
        as="form"
        w={"fit-content"}
        onSubmit={handleSubmit(onSubmit, onError)}
        {...rest}
      >
        {children}
      </Box>
    </FormProvider>
  );
}

export const ConnectForm = ({ children }: any) => {
  const methods = useFormContext();

  return children({ ...methods });
};

export const FormLayout = ({ children, heading, ...rest }: FormLayoutProps) => {
  return (
    <Form {...rest}>
      <InternalFormLayout>{children}</InternalFormLayout>
    </Form>
  );
};
