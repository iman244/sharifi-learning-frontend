export function alphabeticallySort(a: any, b: any, field: string) {
  const aValue = a[field];
  const bValue = b[field];

  if (aValue < bValue) {
    return -1;
  }
  if (aValue > bValue) {
    return 1;
  }
  return 0;
}
