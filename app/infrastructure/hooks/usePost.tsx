import { useMutation } from "react-query";
import { usePostHook } from "../types/restTypes";
import apiClient from "../http-common";
import axios, { AxiosError, AxiosResponse } from "axios";
import { digitsEnToFa } from "@persian-tools/persian-tools";
import { Box, useToast } from "@chakra-ui/react";

const usePost: usePostHook = (
  url,
  options = {
    method: "post",
  }
) => {
  const {
    ContentType,
    loadingState,
    onSettledF,
    case200,
    case201,
    case401,
    caseError,
    method,
  } = options;
  const toast = useToast();
  const POST = useMutation(
    async (data) => {
      var Token = localStorage.getItem("Token");

      switch (method) {
        case "post":
          return await apiClient.post(url, data, {
            headers: {
              "Content-Type": ContentType,
              Authorization: `Token ${Token}`,
            },
          });
        case "put":
          return await apiClient.put(url, data, {
            headers: {
              "Content-Type": ContentType,
              Authorization: `Token ${Token}`,
            },
          });
        case "delete":
          return await apiClient.delete(url, {
            data,
            headers: {
              Authorization: `Token ${Token}`,
            },
          });

        default:
          return await apiClient.post(url, data, {
            headers: {
              "Content-Type": ContentType,
              Authorization: `Token ${Token}`,
            },
          });
      }
    },
    {
      onSettled(data, error, variables, context) {
        loadingState && loadingState[1](false);
        onSettledF && onSettledF(data, error, variables, context);
      },
      onSuccess(d, variables, context): any {
        const { status } = d;
        const data = d.data;
        switch (status) {
          case 200:
            case200 && case200(data, d, variables, context);
            break;
          case 201:
            case201 && case201(data, d, variables, context);
            break;
          case 401:
            case401 && case401(data, d, variables, context);
            break;
          default:
            break;
        }
        if (typeof data.message === "string") {
          toast({
            position: "bottom",
            duration: 3000,
            status: "info",
            description: digitsEnToFa(data.message),
          });
        } else if (Array.isArray(data.message)) {
          data.message.forEach((message: string) =>
            toast({
              position: "bottom",
              duration: 3000,
              status: "info",
              description: digitsEnToFa(message),
            })
          );
        }
      },
      onError(error: AxiosError<any, any>, variables, context) {
        loadingState && loadingState[1](false);
        if (axios.isAxiosError(error)) {
          const { status, response } = error;
          const d = response?.data;
          const { data } = d;
          switch (status) {
            case 401:
              if (case401) {
                case401(data, d, variables, context);
              }
              break;
            default:
              break;
          }
        }

        false && console.log(error.response!.data.message);

        if (caseError) {
          caseError(error, variables, context);
        } else if (
          error.response &&
          error.response.data &&
          error.response.data.message
        ) {
          if (typeof error.response.data.message === "string") {
            toast({
              position: "bottom",
              duration: 3000,
              status: "error",
              description: digitsEnToFa(error.response!.data.message),
            });
          } else if (Array.isArray(error.response.data.message)) {
            error.response.data.message.forEach((message: string) =>
              toast({
                position: "bottom",
                status: "error",
                description: digitsEnToFa(message),
              })
            );
          }
        }
      },
    }
  );
  return POST;
};

export default usePost;
