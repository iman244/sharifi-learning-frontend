import React, { useEffect, useReducer, useState } from "react";
import { simpleQuery } from "../types/inf_frontTypes";
import useGet from "./useGet";
import { UseQueryResult } from "react-query";
import { AxiosResponse } from "axios";
import { useRouter } from "next/navigation";

interface options<D, E> {
  enabled?: boolean;
  log?: boolean;
  keys: any[];
  defaultValue?: D;
  case200?: (data: any) => D;
  case401?: (data: any) => E;
  case404?: (data: any) => void;
}

function reducer(state: number, action: { type: "refresh" }) {
  switch (action.type) {
    case "refresh":
      return state + 1;
  }
}

// type container<D, E> = (url: string, options: options<D, E>) => query<D>;

export function useContainer<D, E = any>(
  url: string,
  options: options<D, E>
): {
  simpleQuery: simpleQuery<D | E | undefined>;
  query: UseQueryResult<AxiosResponse<any, any>, any>;
  refreshGET: () => void;
} {
  const [refreshToken, dispatch] = useReducer(reducer, 0);

  const refreshGET = () => dispatch({ type: "refresh" });

  const router = useRouter();
  const [dataQuery, setDataQuery] = useState<simpleQuery<D | E | undefined>>({
    data: options.defaultValue,
    status: "loading",
  });

  options.log &&
    console.log(
      options.keys,
      "enabled",
      options.enabled !== undefined ? options.enabled : true
    );
  const [data, setData] = useState<D | E | undefined>(options.defaultValue);
  const GET = useGet(url, [...options.keys, refreshToken], {
    enabled: options.enabled !== undefined ? options.enabled : true,
    log: options.log,
    case200: (data) => {
      options.log &&
        console.log(`useContainer ${options.keys} default case200`, data);
      if (options.case200) {
        const pData = options.case200(data);
        setData(pData);
      } else {
        setData(data);
      }
    },
    case401: (data) => {
      if (options.case401) {
        const pData = options.case401(data);
        setData(pData);
      } else {
        // router("/authenticate/sign-in");
      }
    },
    case404: (data) =>{
      options.case404 && options.case404(data)
    },
  });

  useEffect(() => {
    options.log && console.log("useCotainer GET:", GET);
    setDataQuery({ data, status: GET.status });
  }, [data, GET.status]);

  return { simpleQuery: dataQuery, query: GET, refreshGET };
}
