import axios from "axios";
import apiClient from "../http-common";
import { useQuery } from "react-query";
import {
  GETerrorThenFunctions,
  GETsuccessThenFunctions,
  GETunknownThenFunctions,
} from "../types/restTypes";

export default function useGet(
  url: string,
  querykeys: any[],
  options: {
    enabled?: boolean;
    log?: boolean;
    onSettledF?: GETunknownThenFunctions;
    case200?: GETsuccessThenFunctions;
    case201?: GETsuccessThenFunctions;
    case401?: GETsuccessThenFunctions;
    case404?: GETsuccessThenFunctions;
    caseError?: GETerrorThenFunctions;
  } = { enabled: true, log: false }
) {
  var Q;

  var { onSettledF, case200, case201, case401, case404, caseError } = options;

  Q = useQuery(
    [...querykeys],
    () => {
      var Token = localStorage.getItem("Token");
      return apiClient.get(url, {
        headers: {
          Authorization: `Token ${Token}`,
        },
      });
    },
    {
      enabled: options.enabled,
      retry: 2,
      onSettled(data, error) {
        onSettledF && onSettledF(data, error);
      },
      onSuccess(d): any {
        options.log && console.log(`useGet ${querykeys}  onSuccess`, d);
        const { status, data } = d;
        switch (status) {
          case 200:
            case200 && case200(data);
            break;
          case 201:
            case201 && case201(data);
            break;
          case 401:
            case401 && case401(data);
            break;
          default:
            break;
        }
      },
      onError(error: any) {
        if (axios.isAxiosError(error)) {
          options.log &&
            console.log(
              "useGet onError axios.isAxiosError(error)===true error:",
              error
            );
          if (error.response) {
            const { response, status } = error;
            switch (status) {
              case 401:
                case401 && case401(response);
                break;
              case 404:
                case404 && case404(response);
                break;
              default:
                break;
            }
          } else if (error.code === "ERR_NETWORK") {
            console.error("عدم اتصال به شبکه");
          }
        }

        caseError && caseError(error);
      },
    }
  );
  return Q;
}
