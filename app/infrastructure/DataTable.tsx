import React, { Fragment, useState } from "react";
import {
  query,
  simpleQuery,
  tableHeading,
  tableHeadingItem,
} from "./types/inf_frontTypes";
import QueryWindow from "./QueryWindow";
import {
  Button,
  Center,
  Flex,
  Heading,
  Icon,
  Modal,
  ModalContent,
  ModalOverlay,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useDisclosure,
} from "@chakra-ui/react";
import { DeleteBtn, SubmitButton } from "./FormComponents";
import { CiEdit } from "react-icons/ci";
import { FaCheck } from "react-icons/fa";
import Form from "./Form";
import { MdPublishedWithChanges } from "react-icons/md";
import { alphabeticallySort } from "./functions";
import { FaPlus } from "react-icons/fa";

export const DataTable = ({
  query,
  simpleQuery,
  tableH,
  refreshGET,
  url,
}: {
  query: query<any>;
  simpleQuery: simpleQuery<any>;
  tableH: tableHeading;
  refreshGET: () => void;
  url: string;
}) => {
  const loadingState = useState(false);
  const { isOpen, onClose, onOpen } = useDisclosure();
  return (
    <QueryWindow query={query}>
      <Flex
        bgColor={"white"}
        padding={"24px"}
        flexDir={"column"}
        gap={"12px"}
        dir="rtl"
      >
        {url && (
          <Button
            w={"fit-content"}
            onClick={onOpen}
            leftIcon={<Icon as={FaPlus} />}
          >
            آیتم جدید
          </Button>
        )}
        <TableContainer
          dir="rtl"
          width={"100%"}
          overflow={"visible"}
          overflowY={"visible"}
        >
          <Table>
            <Thead>
              <Tr>
                {tableH.map((columnConfig, index) => {
                  const columnName =
                    typeof columnConfig === "string"
                      ? columnConfig
                      : columnConfig.label;

                  return (
                    <Th key={index} textAlign={"center"}>
                      {columnName}
                    </Th>
                  );
                })}
                <Th textAlign={"center"}>ویرایش</Th>
                <Th textAlign={"center"}>حذف</Th>
              </Tr>
            </Thead>
            <Tbody>
              {simpleQuery?.data?.length === 0 && (
                <Text mt={"12px"}>موردی وجود ندارد</Text>
              )}

              {simpleQuery &&
                simpleQuery?.data
                  ?.sort((a: any, b: any) =>
                    alphabeticallySort(
                      a,
                      b,
                      typeof tableH[0] === "string"
                        ? tableH[0]
                        : tableH[0].value
                    )
                  )
                  .map((item: any, index: number) => (
                    <Row
                      item={item}
                      key={index}
                      onSettledF={refreshGET}
                      tableH={tableH}
                      url={url}
                    />
                  ))}
            </Tbody>
          </Table>
        </TableContainer>
        {url && (
          <CreateModal
            isOpen={isOpen}
            onClose={onClose}
            url={url}
            tableH={tableH}
            onSettledF={() => {
              refreshGET && refreshGET();
              onClose();
            }}
          />
        )}
      </Flex>
    </QueryWindow>
  );
};

const Row = ({
  item,
  tableH,
  url,
  onSettledF,
}: {
  item: any;
  tableH: tableHeading;
  url?: string;
  onSettledF?: () => void;
}) => {
  const [editMode, setEditMode] = useState(false);

  let pureUrl = url?.slice(0, url?.indexOf("?"));

  // udURL = update and delete url
  const udURL = `${pureUrl}/${
    typeof tableH[0] === "string" ? item[tableH[0]] : item[tableH[0].value]
  }`;
  return (
    <Tr>
      {tableH.map((columnConfig, index) => (
        <Data
          key={index}
          columnConfig={columnConfig}
          editMode={editMode}
          item={item}
          onSettledF={onSettledF}
          url={udURL}
        />
      ))}

      {url && (
        <>
          <Td>
            <Center mt={editMode ? "28px" : ""}>
              <Button
                onClick={() => setEditMode(!editMode)}
                bgColor={editMode ? "green.300" : ""}
                _hover={
                  editMode ? { bgColor: "green" } : { bgColor: "gray.200" }
                }
              >
                <Icon
                  as={editMode ? FaCheck : CiEdit}
                  color={editMode ? "white" : ""}
                />
              </Button>
            </Center>
          </Td>
          <Td>
            <Center mt={editMode ? "28px" : ""}>
              <DeleteBtn url={udURL} onSettledF={onSettledF} />
            </Center>
          </Td>
        </>
      )}
    </Tr>
  );
};

const Data = ({
  columnConfig,
  item,
  editMode,
  onSettledF,
  url,
}: {
  columnConfig: string | tableHeadingItem;
  item: any;
  editMode: boolean;
  onSettledF?: () => void;
  url?: string;
}) => {
  const isString = typeof columnConfig === "string";
  const columnName = isString ? columnConfig : columnConfig.label;
  const columnRealName = isString ? columnConfig : columnConfig.value;
  const displayValue = Array.isArray(item[columnRealName])
    ? item[columnRealName].join(" | ")
    : `${item[columnRealName]}`;
  const loadingState = useState(false);

  return (
    <Td textAlign={"center"} key={columnName} h={"80px"}>
      {isString ? (
        displayValue
      ) : url ? (
        editMode ? (
          columnConfig.editComponent ? (
            <Form
              selectHandler
              method="patch"
              loadingState={loadingState}
              url={url}
              onSettledF={onSettledF}
              height={"100%"}
            >
              <Flex flexDir={"column"} gap={"8px"}>
                <Text>{displayValue}</Text>
                <Flex alignItems={"flex-end"} gap={"12px"} height={"100%"}>
                  {columnConfig.editComponent}
                  <SubmitButton isLoading={loadingState[0]} simpleLoading>
                    <Icon as={MdPublishedWithChanges} />
                  </SubmitButton>
                </Flex>
              </Flex>
            </Form>
          ) : (
            displayValue
          )
        ) : (
          displayValue
        )
      ) : (
        displayValue
      )}
    </Td>
  );
};

const CreateModal = ({
  isOpen,
  onClose,
  url,
  tableH,
  onSettledF,
}: {
  isOpen: boolean;
  onClose: () => void;
  url: string;
  tableH: tableHeading;
  onSettledF?: () => void;
}) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent padding={"18px"} dir="rtl">
        <Flex w={"100%"}>
          <CreateItem url={url} tableH={tableH} onSettledF={onSettledF} />
        </Flex>
      </ModalContent>
    </Modal>
  );
};

const CreateItem = ({
  url,
  tableH,
  onSettledF,
}: {
  url: string;
  tableH: tableHeading;
  onSettledF?: () => void;
}) => {
  const loadingState = useState(false);
  let pureUrl = url?.slice(0, url?.indexOf("?"));

  return (
    <Form
      url={pureUrl}
      selectHandler
      loadingState={loadingState}
      onSettledF={onSettledF}
      log
    >
      <Flex
        alignItems={"center"}
        gap={"18px"}
        justifyContent={"center"}
        w={"100%"}
      >
        <Heading>آیتم جدید</Heading>
        <SubmitButton isLoading={loadingState[0]} w={"100px"}>
          ایجاد
        </SubmitButton>
      </Flex>
      <Flex dir="rtl" flexDir={"column"} w={"216px"} gap={"12px"}>
        {tableH.map((columnConfig, index) => {
          const isString = typeof columnConfig === "string";
          return (
            <>
              {isString ? (
                <Text>{`ورودی ${columnConfig} تعریف نشده است`}</Text>
              ) : columnConfig.editComponent ? (
                columnConfig.editComponent
              ) : (
                <Text>{`ورودی ${columnConfig} تعریف نشده است`}</Text>
              )}
            </>
          );
        })}
      </Flex>
    </Form>
  );
};
