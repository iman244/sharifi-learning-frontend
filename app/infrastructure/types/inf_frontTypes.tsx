import {
  ButtonProps,
  ChakraProps,
  ComponentWithAs,
  InputProps,
  LinkProps,
  StyleProps,
  SystemCSSProperties,
  SystemProps,
} from "@chakra-ui/react";
import { AxiosError, AxiosResponse } from "axios";
import { CSSProperties, ReactElement } from "react";
import { FieldValues, RegisterOptions } from "react-hook-form";
import { IconType } from "react-icons";
import { QueryStatus, UseQueryResult } from "react-query";
import { GroupBase, OptionsOrGroups, StylesConfig } from "react-select";

export interface simpleQuery<T> {
  data: T;
  status: QueryStatus;
}

export type query<T = any> =
  | UseQueryResult<AxiosResponse<T, any>, AxiosError>
  | undefined;

// export type QueryWindowStatus = QueryStatus | "not connected to container";

export interface CompilicatedInputProps extends ChakraProps {
  register: any;
  name: string;
  placeholder?: string;
  rules?:
    | Omit<
        RegisterOptions<FieldValues, any>,
        "disabled" | "valueAsNumber" | "valueAsDate" | "setValueAs"
      >
    | undefined;
  type?: React.HTMLInputTypeAttribute | "textarea";
}

export interface CompilicatedSelectProps extends ChakraProps {
  register: any;
  name: string;
  placeholder: string;
  rules:
    | Omit<
        RegisterOptions<FieldValues, any>,
        "disabled" | "valueAsNumber" | "valueAsDate" | "setValueAs"
      >
    | undefined;
  options: OptionsOrGroups<unknown, GroupBase<unknown>>;
  selectStyles?: StylesConfig<unknown, false, GroupBase<unknown>> | undefined;
  isClearable?: boolean;
  defaultValue?: any;
  isMulti?: boolean;
}

export interface CompilicatedCreatableSelectProps
  extends CompilicatedSelectProps {
  isDisabled?: boolean;
  onCreateOption: ((inputValue: string) => void) | undefined;
}

export type selectOptions = OptionsOrGroups<unknown, GroupBase<unknown>>;

export interface SubmitButtonProps
  extends ButtonProps,
    StyleProps,
    ChakraProps {
  children?: any;
  variant?: string;
  isLoading: boolean;
  loadingTest?: boolean;
  spinnerColor?: string;
  simpleLoading?: boolean;
  icon?: ReactElement<any, any>;
}

export interface reactSelectItem<L = any, V = any> {
  label: string;
  value: V;
}
export interface page {
  path: string;
  name?: string;
}

export type pages = page[];

export interface pageCSS {
  navbarH: string;
  marginPage: string;
}

export interface navbarCSS {
  gap: string;
  paddingY: string;
}

export type PageLink = ({ page }: { page: page }) => React.JSX.Element;

export interface tableHeadingItem {
  label: string;
  value: string;
  editComponent?: React.JSX.Element;
}

export type tableHeading = (string | tableHeadingItem)[];

export interface FormInputUIProps extends StyleProps {
  placeholder?: string;
  type: React.HTMLInputTypeAttribute | "textarea";
}

export interface containerInput {
  simpleQuery: simpleQuery<any>;
  query: query<any>;
  refreshGET: () => void;
}

export interface NetworkContainerInput extends containerInput {
  url: string;
}
