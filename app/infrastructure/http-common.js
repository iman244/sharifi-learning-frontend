import axios from "axios";
const production = true
const admin_domain = 'https://admin.ieltsonlineshopping.ir'
const website_domain = `https://www.ieltsonlineshopping.ir`
export const IP = production ? admin_domain : "http://127.0.0.1"
export const backendPort = production ? '80' : '8000'
export const frontPort = '3000'
export const frontIP = production ?  website_domain :`${IP}:${frontPort}/`;
export const backendIP = production ? `${IP}/api` : `${IP}:${backendPort}/api`

export default axios.create({
  baseURL: `${backendIP}/`,
  withCredentials: true,
});
