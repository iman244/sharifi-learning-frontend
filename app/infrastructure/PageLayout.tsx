"use client";
import { Box, Flex, Link, Spacer } from "@chakra-ui/react";
import React from "react";
import { PageLink, navbarCSS, pageCSS, pages } from "./types/inf_frontTypes";
import { CustomPageLink } from "../components/CustomPageLink";
import { Footer } from "../components/Footer";

export const PageLayout = ({
  children,
  navbar,
  pages,
}: {
  children: any;
  navbar?: React.JSX.Element;
  pages?: pages;
}) => {
  const navbarCSS: navbarCSS = {
    gap: "12px",
    paddingY: "12px",
  };

  const pageCSS: pageCSS = {
    navbarH: "70px",
    marginPage: "24px",
  };

  return (
    <Flex flexDir={"column"} minH={"100vh"} bgColor={'gray.50'}>
      {navbar ? (
        <Box
          as="nav"
          bgColor={"white"}
          w={"100%"}
          h={pageCSS.navbarH}
          top={"0"}
          position={"fixed"}
          paddingX={{ base: "12px", md: pageCSS.marginPage }}
          paddingY={navbarCSS.paddingY}
          zIndex={1000}
          boxShadow={"md"}
        >
          {navbar}
        </Box>
      ) : (
        pages && (
          <Navbar
            navbarCSS={navbarCSS}
            pageCSS={pageCSS}
            pages={pages}
            PageLink={CustomPageLink}
          />
        )
      )}
      <Flex
        paddingY={"36px"}
        marginTop={pageCSS.navbarH}
        marginX={pageCSS.marginPage}
        flexDir={"column"}
      >
        {children}
      </Flex>
      <Spacer />
      <Footer />
    </Flex>
  );
};

const Navbar = ({
  navbarCSS,
  pageCSS,
  pages,
  PageLink,
}: {
  navbarCSS: navbarCSS;
  pageCSS: pageCSS;
  pages: pages;
  PageLink?: PageLink;
}) => {
  return (
    <Flex
      bgColor={"gray.100"}
      h={pageCSS.navbarH}
      top={"0"}
      position={"fixed"}
      alignItems={"center"}
      gap={navbarCSS.gap}
      paddingY={navbarCSS.paddingY}
      paddingX={pageCSS.marginPage}
    >
      {pages.map((page) => {
        return PageLink ? (
          <PageLink key={page.path} page={page} />
        ) : (
          <Link href={page.path}>{page.name}</Link>
        );
      })}
    </Flex>
  );
};
