'use client'
import { Auth } from "@/app/auth/Auth";
import { Authorization } from "@/app/infrastructure/Authorization";
import { user } from "@/app/types/backend/entities";
import { Flex, Heading, Text } from "@chakra-ui/react";
import { PaymentInfo } from "./components/PaymentInfo";
import { CustomPageLink } from "@/app/components/CustomPageLink";
import { digitsEnToFa } from "@persian-tools/persian-tools";

export default function PaymentsPage() {
  return (
    <Authorization
      render={(user: user) => { 
        const payments = user.payments.filter((payment)=> {
          if (payment.products.length === 0 && payment.products[0].price === 0) {
            return false
          } else {
            return true
          }
        })
        console.log("payments", user.payments)
        return (
        <Flex flexDir={"column"} gap={"36px"} w={"100%"} dir="rtl">
          {payments.length > 0 ? (
            <>
              <Flex gap={"12px"}>
                <Text>شماره تماس شما:</Text>
                <Text dir="ltr">{user.phone_number}</Text>
              </Flex>
              <CustomPageLink page={{path: 'profile'}}>محصولات خریداری شده</CustomPageLink>
              <Heading>پرداخت‌های شما</Heading>
              <Flex gap={"24px"}>
                {payments.map((payment, index) => {
                  return <PaymentInfo key={index} payment={payment} />;
                })}
              </Flex>
            </>
          ) : (
            <>
            <Text>
              پرداختی برای شما ثبت نشده است، اگر پرداختی داشته‌اید که این صفحه
              به شما نشان نمی‌دهد لطفاً با پشتیبانی تماس بگیرید
            </Text>
            <Text>
              شماره تماس پشتیبانی: {digitsEnToFa("09909378965")}
            </Text>
            </>
          )}
        </Flex>
      )}}
      notValid={
        <Flex justifyContent={"center"} w={"100%"}>
          <Auth nextPath={"/profile/payments"} />
        </Flex>
      }
    />
  );
}
