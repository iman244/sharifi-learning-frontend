import { payment } from "@/app/types/backend/entities";
import { Flex, Icon, Spinner, Text, useToast } from "@chakra-ui/react";
import { addCommas, digitsEnToFa } from "@persian-tools/persian-tools";
import NextLink from "next/link";
import React, { useEffect, useState } from "react";
import { BiCheck } from "react-icons/bi";
import { IoIosCheckmarkCircle, IoIosCloseCircle } from "react-icons/io";
const persianDate = require("persian-date");

export const PaymentInfo = ({ payment }: { payment: payment }) => {
  const date = new persianDate.unix(Number(payment.paymentStartTime));
  const [isLoadingPaymentPage, setIsLoadingPaymentPage] = useState(false);


  return (
    <Flex
      as={NextLink}
      href={`/payment?paymentId=${payment.id}`}
      alignItems={"center"}
      flexDir={"column"}
      borderRadius={"16px"}
      overflow={"hidden"}
      boxShadow={"md"}
      bgColor={"white"}
      _hover={{ boxShadow: "2xl" }}
      transition={"all 0.3s linear"}
      position={"relative"}
      padding={"24px"}
      gap={"16px"}
      onClick={() => {
        setIsLoadingPaymentPage(true);
      }}
    >
      {isLoadingPaymentPage && <CardLoading />}
      <StatusInfo status={payment.status} />
      <AmountInfo amount={payment.amount} />
      <DateInfo date={date.toLocale("fa").format()} />
    </Flex>
  );
};

const StatusInfo = ({ status }: { status: boolean }) => {
  return (
    <Flex gap={"24px"}>
      <Text>وضعیت:</Text>
      <Flex
        alignItems={"center"}
        justifyContent={"center"}
        gap={"4px"}
        color={status ? "green" : "red"}
      >
        <Icon as={status ? IoIosCheckmarkCircle : IoIosCloseCircle} />
        <Text>{status ? "موفق" : "ناموفق"}</Text>
      </Flex>
    </Flex>
  );
};

const AmountInfo = ({ amount }: { amount: number }) => {
  return (
    <Flex gap={"24px"}>
      <Text>مبلغ:</Text>
      <Flex gap={"8px"}>
        <Text>{digitsEnToFa(addCommas(amount))}</Text>
        <Text>تومان</Text>
      </Flex>
    </Flex>
  );
};
const DateInfo = ({ date }: { date: string }) => {
  return <Text>{date}</Text>;
};

export const CardLoading = () => {
  return (
    <Flex
      alignItems={"center"}
      justifyContent={"center"}
      zIndex={1}
      position={"absolute"}
      width={"100%"}
      height={"100%"}
      bgColor={"gray.500"}
      opacity={0.7}
      top={0}
    >
      <Spinner />
    </Flex>
  );
};
