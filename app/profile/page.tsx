"use client";
import React, { useContext } from "react";
import { Authorization } from "../infrastructure/Authorization";
import { Button, Card, Flex, Heading, Link, Text } from "@chakra-ui/react";
import { UserContext } from "../logics/user";
import { user } from "../types/backend/entities";
import { ProductCard_DownloadFile } from "./components/ProductCard_DownloadFile";
import { Auth } from "../auth/Auth";
import NextLink from "next/link";
import { CustomPageLink } from "../components/CustomPageLink";
import { digitsEnToFa } from "@persian-tools/persian-tools";

export default function ProfilePage() {
  return (
    <Authorization
      render={(user: user) => (
        <Flex flexDir={"column"} gap={"36px"} w={"100%"} dir="rtl">
          {user.products.length > 0 ? (
            <>
              <Flex gap={"12px"} bg={'blue.100'} color={'blue.600'} w={'fit-content'} padding={'8px 12px'} boxShadow={'md'} borderRadius={'8px'}>
                <Text fontWeight={500}>شماره تماس شما:</Text>
                <Text dir="ltr" >{digitsEnToFa(user.phone_number)}</Text>
              </Flex>
            <CustomPageLink page={{path: 'profile/payments'}}>پرداخت‌های شما</CustomPageLink>
              <Heading>محصولات خریداری شده</Heading>
              <Flex gap={"24px"}>
                {user.products.map((products_includeFile, index) => {
                  return (
                    <ProductCard_DownloadFile key={index} product={products_includeFile} />
                  );
                })}
              </Flex>
            </>
          ) : (
            <Text>
              شما محصولی خریداری نکرده‌‍اید! برای خرید{" "}
              <Link
                as={NextLink}
                href="/store"
                color={"blue"}
                textDecor={"underline"}
              >
                اینجا
              </Link>{" "}
              کلیک کنید
            </Text>
          )}
        </Flex>
      )}
      notValid={
        <Flex justifyContent={"center"} w={"100%"}>
          <Auth nextPath={"/profile"} />
        </Flex>
      }
    />
  );
}
