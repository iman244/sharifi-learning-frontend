import React, { MouseEvent, useContext, useEffect, useState } from "react";

import { Button, Flex, Heading, Icon, Link, Text } from "@chakra-ui/react";
import Image from "next/image";
import { addCommas, digitsEnToFa } from "@persian-tools/persian-tools";
import { FaCartPlus } from "react-icons/fa";
import { BsCartDashFill, BsFillCartCheckFill } from "react-icons/bs";
import { useRouter } from "next/navigation";
import { product, product_includeFile } from "@/app/types/backend/entities";
import { CardLoading } from "@/app/components/CardLoading";
import { CartContext } from "@/app/logics/cart";

export const ProductCard_DownloadFile = ({
  product,
}: {
  product: product_includeFile;
}) => {
  const router = useRouter();
  const [isLoadingCartPage, setIsLoadingCartPage] = useState(false);
  const [isLoadingProductPage, setIsLoadingProductPage] = useState(false);

  return (
    <Flex
      w={"300px"}
      alignItems={"center"}
      flexDir={"column"}
      borderRadius={"16px"}
      overflow={"hidden"}
      boxShadow={"md"}
      bgColor={"white"}
      _hover={{ boxShadow: "2xl" }}
      transition={"all 0.3s linear"}
      position={"relative"}
    >
      {isLoadingProductPage && <CardLoading />}
      <Flex w={"300px"} height={"300px"} bgColor={"gray.200"}>
        <Image
          style={{ objectFit: "contain" }}
          src={product.image.file}
          alt={product.image.name}
          width={300}
          height={300}
        />
      </Flex>
      <Flex
        alignItems={"center"}
        gap={"12px"}
        flexDir={"column"}
        padding={"12px"}
      >
        <Heading
          onClick={() => {
            setIsLoadingProductPage(true);
            router.push(`store/${product.id}`);
          }}
          cursor={"pointer"}
          _hover={{
            textDecor: "underline",
          }}
        >
          {product.name}
        </Heading>
        {/* <Text variant={"description"}>
          {product.content.length >= 15
            ? `${product.content.slice(0, 14)}...`
            : product.content}
        </Text> */}

        <Button
          as={Link}
          href={product.file.file}
          target="_blank"
          w={"216px"}
          colorScheme="blue"
        >
          دانلود {product.name}
        </Button>
      </Flex>
    </Flex>
  );
};
