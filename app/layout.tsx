import type { Metadata } from "next";
import "./globals.css";
import "./infrastructure/infrastructure.global.css";
import Providers from "./provider";
import { PageLayout } from "./infrastructure/PageLayout";
import { pages } from "./infrastructure/types/inf_frontTypes";
import { Navbar } from "./components/Navbar/Navbar";
import { useEffect } from "react";
import { useToast } from "@chakra-ui/react";
import Head from "next/head";

export const S3_Bucket_href =
  "https://sharifi-learning.s3.ir-thr-at1.arvanstorage.ir";

export const metadata: Metadata = {
  title: "IELTS Online Shopping",
  other: {
    enamad: "817729"
  }
  
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {

 
  return (
    <html
      style={{
        minHeight: "100vh",
        // backgroundColor: "#00008B",
        backgroundPosition: "center",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
      }}
    >
      <body style={{ background: "transparent" }}>
        <Providers>
          <Head>
            <link rel="shortcut icon" href="/favicon.ico" />
          </Head>
          <PageLayout navbar={<Navbar />}>{children}</PageLayout>
        </Providers>
      </body>
    </html>
  );
}
